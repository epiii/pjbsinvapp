import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity, TextInput, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button, FAB, List, DataTable } from 'react-native-paper';
// import { useNavigation } from '@react-navigation/native';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const CardDetail = ({ notifTitle, notifDesc }) => {
    return (
        <View style={styles.cardDetail}>
            <Text style={styles.notifTitle}>{notifTitle}</Text>
            <Text style={styles.notifDesc}>{notifDesc}</Text>
        </View>
    );
}


function Component({ navigation, route }) {
    React.useEffect(() => {
        if (route.params?.notifTitle) {
            // For example, send the post to the server
        }
    }, [route.params?.notifTitle]);


    return (
        <SafeAreaView
            style={styles.container}
        >
            <ScrollView
                contentContainerStyle={{ flexGrow: 1 }}
            >
                <View style={{
                    height: 1.3 * hScreen
                }}>
                    <Image style={styles.bgImage} source={IMAGES.PETIR} />
                    <Image style={[styles.bgImage, styles.overlayImage]} source={IMAGES.PEMBANGKIT} />

                    <View style={styles.content}>
                        <CardDetail
                            notifTitle={route.params?.notifTitle}
                            notifDesc={route.params?.notifDesc}
                        />
                    </View>

                </View>

            </ScrollView>
        </SafeAreaView>
    )
}

export default Component
