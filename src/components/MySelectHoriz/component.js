import React, { useState, useContext, useEffect } from 'react'
import {
    Dimensions,
    TextInput,
    View,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native'
import styles from './style'
import IconFA from 'react-native-vector-icons/FontAwesome'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const component = ({ name, option, isFullWidth, textAlign, value, val, placeholder }) => {
    const [valuex, setValuex] = useState(0);
    const [labelx, setLabelx] = useState('');
    const [opt, setOpt] = useState([])

    useEffect(() => {
        console.log(valuex, val)
        setOpt(option)
        // setValuex(value)
    }, [])

    const right = () => {
        console.log('option', option.length)
        console.log('bef', valuex)
        console.log('opt', valuex)

        if (parseInt(valuex) < (option.length)-1) {
            setValuex((parseInt(valuex) + 1).toString())
        }
        // console.log('after', valuex)
    }

    const left = () => {
        console.log('bef', valuex)
        setValuex((parseInt(valuex) > 0 ? parseInt(valuex) - 1 : 0).toString())
        console.log('after', valuex)
    }

    const ButtonNum = ({ type }) => (
        <TouchableHighlight
            underlayColor={'#5CC652'}
            style={{
                flex: 3,
                paddingTop: .03 * wScreen,
                paddingHorizontal: .01 * wScreen,
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                // backgroundColor: 'yellow',
            }}
            onPress={() => (type == 'right' ? right() : left())}
        >
            <IconFA
                name={type == 'right' ? 'chevron-right' : 'chevron-left'}
                style={{
                    flex: 1,
                }}
                size={.05 * wScreen}
                color={'#0B2E6A'}
            />
        </TouchableHighlight>
    )

    return (
        <View style={{
            flexDirection: 'row',
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderRadius: .03 * wScreen,
            width: isFullWidth ? '100%' : '50%'
        }}>
            <ButtonNum type={'left'} />
            <TextInput
                style={[styles.txtInput, {
                    // height: .1 * wScreen,
                    textAlign: textAlign ? textAlign : 'right',
                    width: '70%',
                    fontSize: .05 * wScreen,
                    fontWeight: 'bold',
                    // backgroundColor: 'red'
                    flex: 3
                }]
                }
                keyboardType={'numeric'}
                name={name}
                // value={option[0].label}
                value={option[valuex].label}
                onChangeText={(text) => setValuex(text)}
                placeholder={placeholder}
                placeholderTextColor="grey"
            />
            <ButtonNum type={'right'} />
            {/* <ButtonNum onPress={() => left()} /> */}
        </View>
    )
}


export default component
