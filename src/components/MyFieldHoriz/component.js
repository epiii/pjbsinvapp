import React from 'react'
import { View } from 'react-native'
import styles from './style'

const component = ({ leftComponent, rightComponent, leftFlex, rightFlex }) => (
    <View style={styles.fieldAreaHoriz} >
        <View style={[styles.leftComponent, { flex: leftFlex ? leftFlex : 1 }]}>
            {leftComponent ? leftComponent : ''}
        </View>
        <View style={[styles.rightComponent, { flex: rightFlex ? rightFlex : 1 }]}>
            {rightComponent ? rightComponent : ''}
        </View>
    </View>
)

export default component
