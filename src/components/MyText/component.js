import React from 'react'
import { View, Text } from 'react-native'
import styles from './style'
import { wScreen } from '../../configs/Settings'

const component = ({ text, fgColor, bgColor, textAlign, isBold }) => (
    <View>
        <Text style={{
            // marginVertical: .005 * wScreen,
            fontWeight: isBold ? 'bold' : 'normal',
            fontSize: isBold ? (.042 * wScreen) : (.037 * wScreen),
            color: fgColor ? fgColor : 'white',
            textAlign: textAlign ? textAlign : 'justify',
            backgroundColor: bgColor ? bgColor : 'transparent'
        }}>{text}</Text>
    </View>
)


export default component
