import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FAB } from 'react-native-paper';
import { FlatGrid, SectionGrid } from 'react-native-super-grid'
import IconFA from 'react-native-vector-icons/FontAwesome'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const ListTool = ({ items }) => {
    const navigation = useNavigation();

    return (
        // <View style={{
        //     // backgroundColor:'red',
        //     // paddingVertical: .05 * wScreen
        // }}>
        <SectionGrid
            // itemDimension={130}
            itemDimension={.2 * wScreen}
            data={items}
            style={styles.gridView}
            // staticDimension={2000}
            // fixed
            sections={[
                {
                    title: 'Peminjaman',
                    data: items.slice(0, 2),
                    color: '#FFAC30',
                },
                {
                    title: 'Pengembalian',
                    data: items.slice(2, 4),
                    color: '#5CC652',
                },
                // {
                //     title: 'Others',
                //     data: items.slice(4, 12),
                //     color: '#EC3247',
                // },
            ]}
            spacing={10}
            // renderItem={({ item }) => (
            renderItem={({ item, section, index }) => (
                <TouchableHighlight underlayColor={section.color} onPress={() => navigation.navigate(item.screen)}>
                    <View style={[styles.itemContainer, { backgroundColor: section.color }]}>
                        <IconFA name={item.icon} color={'white'} size={.09 * wScreen} />
                        <Text style={styles.itemCode}>{item.code}</Text>
                    </View>
                </TouchableHighlight>
            )} renderSectionHeader={({ section }) => (
                <Text style={styles.sectionHeader}>{section.title}</Text>
            )}
        />
        // </View>
    );
}

const items = [
    { name: 'ASBESTOS', code: 'Tool\nProyek', screen: 'PeminjamanToolProyekForm', color:'',icon: 'book' },
    { name: 'ASBESTOS', code: 'Tool\nPusat', screen: 'PeminjamanToolPusatForm', color:'',icon: 'book' },
    { name: 'ASBESTOS', code: 'Tool\nProyek', screen: 'PengembalianToolProyekForm', color:'',icon: 'reply' },
    { name: 'ASBESTOS', code: 'Tool\nPusat', screen: 'PengembalianToolPusatForm', color:'',icon: 'reply' },
]

function Component({ navigation }) {
    // const [items, setItems] = React.useState([
    //     { name: 'ASBESTOS', code: '#7f8c8d', iconscreen:'PeminjamanToolForm',: 'plus' },
    // ]);

    useEffect(() => {
        return () => {
            // cleanup
        }
    }, [])

    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                {/* <SafeAreaView style={styles.safeAreaView}> */}
                {/* <ScrollView contentContainerStyle={{ flexGrow: 1 }} > */}
                <View style={{
                    flex: 1,
                     paddingHorizontal: .05 * wScreen
                }} >
                    <ListTool items={items} />
                </View>
                {/* </ScrollView> */}
                {/* </SafeAreaView> */}

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
