import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width

const styles = StyleSheet.create({
    button: {
        margin: .03 * wScreen,
        padding: .02 * wScreen,
    }
});
export default styles
