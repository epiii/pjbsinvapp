import React, { useContext, useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ImageBackground, TextInput, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button, FAB, DataTable } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage'
import { AuthContext } from '../../providers/AuthContext'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

function Component({ navigation, route }) {
    const [isLoggedIn, setIsLoggedIn] = useState(null)
    const [userType, setUserType] = useState(null)

    const { login } = useContext(AuthContext)
    console.log('login', login)

    useEffect(() => {
        console.log('lgin islogin', isLoggedIn)
        console.log('lgin user', userType)
    }, []);

    const handleClickLogin = async (typex) => {
        try {
            let authObj = {}
            authObj.isLoggedIn = '1'
            authObj.userType = typex

            AsyncStorage.setItem('authInfo', JSON.stringify(authObj)).then(() => login(authObj))
            // await AsyncStorage.setItem('@isLoggedIn', '1').then((logx) => {
            //     await AsyncStorage.setItem('@userType', typex).then((userx) => {
            //         login(logx, userx)
            //     })
            // })
        } catch (e) {
            console.log(e)
            alert('failed to login ')
        }
    }

    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={styles.container} >

                            <View style={styles.sectionTop}>
                                <View style={styles.logoArea}>
                                    <Image style={[styles.bgImage, {
                                    }]} source={require("../../assets/images/logo_pjbs.png")} />
                                    <Text style={styles.txtLabel}>PROMIS</Text>
                                </View>
                            </View>

                            <View style={styles.sectionBottom}>
                                <TextInput
                                    style={styles.txtInput}
                                    placeholder="username"
                                    placeholderTextColor="grey"
                                />
                                <TextInput
                                    style={styles.txtInput}
                                    placeholder="Password"
                                    placeholderTextColor="grey"
                                    secureTextEntry={true}
                                />

                                <View style={{
                                    flexDirection: 'row',
                                    justifyContent: 'space-between'
                                }}>
                                    <View style={{ flex: 1, }}>
                                        <Button
                                            color='white'
                                            style={styles.btnListRencanaPro}
                                            onPress={() => handleClickLogin('2')}
                                        // onPress={() => login()}
                                        >Adm Proyek </Button>
                                    </View>
                                    <View style={{ flex: 1, }}>
                                        <Button
                                            color='white'
                                            style={styles.btnListRencanaPro}
                                            onPress={() => handleClickLogin('1')}
                                        >Adm Pusat </Button>
                                    </View>
                                </View>
                            </View>

                        </View>
                    </ScrollView>
                </SafeAreaView>
            </ImageBackground>
        </ImageBackground>

    )
}

export default Component
