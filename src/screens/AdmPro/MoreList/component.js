import React, { useContext, useReducer, useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation, CommonActions } from '@react-navigation/native';
import { FAB } from 'react-native-paper';
import { FlatGrid, SectionGrid } from 'react-native-super-grid'
import IconFA from 'react-native-vector-icons/FontAwesome'
import AsyncStorage from '@react-native-community/async-storage'
import { AuthContext } from '../../../providers/AuthContext'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height



const ListTool = ({ items }) => {
    const navigation = useNavigation();

    return (
        // <View style={{
        //     // backgroundColor:'red',
        //     // paddingVertical: .05 * wScreen
        // }}>
        <SectionGrid
            // itemDimension={130}
            itemDimension={.2 * wScreen}
            data={items}
            style={styles.gridView}
            // staticDimension={2000}
            // fixed
            sections={[
                {
                    title: '',
                    data: items.slice(0, 3),
                    color: '#5CC652',
                },
                // {
                //     title: 'Reportd',
                //     data: items.slice(3, 4),
                //     color: '#5CC652',
                // },
                // {
                //     title: 'Others',
                //     data: items.slice(4, 12),
                //     color: '#EC3247',
                // },
            ]}
            spacing={10}
            // renderItem={({ item }) => (
            renderItem={({ item, section, index }) => (
                <TouchableHighlight underlayColor={section.color} onPress={() => navigation.navigate(item.screen)}>
                    <View style={[styles.itemContainer, { backgroundColor: section.color }]}>
                        <IconFA name={item.icon} color={'white'} size={.09 * wScreen} />
                        <Text style={styles.itemCode}>{item.code}</Text>
                    </View>
                </TouchableHighlight>
            )} renderSectionHeader={({ section }) => (
                <Text style={styles.sectionHeader}>{section.title}</Text>
            )}
        />
        // </View>
    );
}

const items = [
    // form
    // { name: 'ASBESTOS', code: 'Pinjam Tool Proyek', screen: 'PeminjamanToolForm', icon: 'book' },
    // { name: 'ASBESTOS', code: 'Kembali Tool User', screen: 'PengembalianToolProyekForm', icon: 'reply' },
    // { name: 'ASBESTOS', code: 'Kembali Tool Pusat', screen: 'PengembalianToolPusatForm', icon: 'reply' },

    // report
    { name: 'ASBESTOS', code: 'Gap\nTool', screen: 'ReportGapTool', icon: 'area-chart' },

    // other
    { name: 'ASBESTOS', code: 'Stok Gudang', screen: 'StokGudang', icon: 'cubes' },
]

function Component({ navigation }) {
    // const [items, setItems] = React.useState([
    //     { name: 'ASBESTOS', code: '#7f8c8d', iconscreen:'PeminjamanToolForm',: 'plus' },
    // ]);
    const forceUpdate = useReducer(bool => !bool)[1];
    useEffect(() => {
        return () => {
            // cleanup
        }
    }, [])
    const { logout } = useContext(AuthContext)

    const handleClickLogout = async () => {
        // const navigation = useNavigation();
        try {
            AsyncStorage.clear().then(val => {
                // navigation.dispatch(
                //     CommonActions.reset({
                //         index: 0,
                //         // routes: [{ name: "Home" }],
                //     })
                // );

                // navigation.reset({
                //     routes: [{ name: 'Home' }]
                // });
                // alert('logout successfully')
                logout()
            })
        } catch (e) {
            alert('failed to logout ')
        }
    }
    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                {/* <SafeAreaView style={styles.safeAreaView}> */}
                {/* <ScrollView contentContainerStyle={{ flexGrow: 1 }} > */}
                <View style={{
                    flex: 1,
                    paddingHorizontal: .05 * wScreen
                }} >
                    <ListTool items={items} />
                </View>
                {/* </ScrollView> */}
                {/* </SafeAreaView> */}
                <Button
                    color='white'
                    style={{
                        margin: .3 * wScreen,
                        alignSelf: 'center',
                        alignContent: 'center',
                        backgroundColor: '#FFAC30'
                    }}
                    onPress={() => handleClickLogout()}
                >Logout</Button>
            </ImageBackground>
        </ImageBackground >
    )
}

export default Component
