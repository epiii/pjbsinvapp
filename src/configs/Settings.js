import { Dimensions } from 'react-native'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height


export { wScreen, hScreen }