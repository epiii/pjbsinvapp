import React from 'react'
import {
    Dimensions,
    TextInput,
} from 'react-native'
import styles from './style'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const component = ({ isMultiLine, value, name, textAlign, placeholder, label }) => {
    return (
        <TextInput
            style={
                [styles.txtInput, {
                    textAlign: textAlign ? textAlign : 'left',
                    textAlignVertical: isMultiLine ? 'top' : 'center',
                    width: '100%'
                }]
            }
            numberOfLines={isMultiLine ? 3 : 1}
            multiline={isMultiLine ? true : false}
            name={name}
            value={value}
            placeholder={placeholder}
            placeholderTextColor="grey"
        />
    )
}
export default component
