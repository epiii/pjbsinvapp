import React, { useMemo, useEffect, useState } from 'react'
import { View, Text, Button } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import BottomTabAdmPro from './src/components/BottomTabAdmPro'
import BottomTabAdmPus from './src/components/BottomTabAdmPus'
import { SafeAreaProvider, SafeAreaView } from 'react-native-safe-area-context';
import { Provider as PaperProvider } from 'react-native-paper';
import LoginScreen from './src/screens/Login'
import { createStackNavigator } from '@react-navigation/stack'
import AsyncStorage from '@react-native-community/async-storage'
import { AuthContext } from './src/providers/AuthContext'

const Stack = createStackNavigator()
// 1= adm pusat
// 2= adm proyek
// export const AuthContext = React.createContext(); // added this
// const store = createStore(rootReducer);

// iki offline - first branch 

const initialState = {
  isAuthenticated: false,
  user: null,
  token: null,
};
const reducer = (state, action) => {
  switch (action.type) {
    case 'LOGIN':
      localStorage.setItem('user', JSON.stringify(action.payload.user))
      localStorage.setItem('token', JSON.stringify(action.payload.token))
      return {
        ...state,
        isAuthenticated: true,
        user: action.payload.user,
      };
    case 'LOGOUT':
      localStorage.clear()
      return {
        ...state,
        isAuthenticated: false,
        user: null,
      };

    default:
      return state;
  }
}
const DisplayContent = ({ login, role }) => {
  // console.log('is login', login == undefined)
  // console.log('login', login)
  // console.log('role', role)
  // return false

  if (login === undefined || !login || login == null) {
    // alert('logouted')

    // return false
    return (
      <Stack.Navigator>
        <Stack.Screen
          name="Login"
          component={LoginScreen}
          options={{
            title: '',
            headerShown: false
          }}
        />
      </Stack.Navigator>
    )
  } else {
    // alert('logined')

    // return false
    return (role == '1' ? (<BottomTabAdmPus />) : (<BottomTabAdmPro />))
  }
}

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(null)
  const [userType, setUserType] = useState(null)
  // alert(isLoggedIn)
  useEffect(() => {
    getLogin()
    // alert('yus efek app')
    console.log('app,isLoggedIn var', isLoggedIn)
    console.log('app,userType var', userType)
  }, []);

  getLogin = async () => {
    try {
      AsyncStorage.getItem('authInfo').then((valx) => {
        let authObj = JSON.parse(valx)

        if (authObj) {
          setIsLoggedIn(authObj.isLoggedIn)
          setUserType(authObj.userType)
        }
      }).done();

      // const isLoggedInx =
      // await AsyncStorage.getItem('@isLoggedIn').then((val) => setIsLoggedIn(val))
      // // const userTypex =
      // await AsyncStorage.getItem('@userType').then((val) => setUserType(val))

      // console.log('isLoggedIn app', isLoggedIn)
      // console.log('userType app', userType)
      // console.log('app,isLoggedIn var', isLoggedInx)
      // console.log('app,userType var', userTypex)
    } catch (e) {
      alert('failed get info login')
    }
  }

  const auth = useMemo(
    () => ({
      login: (authObj) => {
        setIsLoggedIn(authObj.isLoggedIn)
        setUserType(authObj.userType)
      },
      logout: () => {
        setIsLoggedIn(null)
        setUserType(null)
        // console.log('logout', isLoggedInx, userTypex)
      },
    }),
    []
  );

  return (
    // <PaperProvider>
    <AuthContext.Provider value={auth} >
      {/* <SafeAreaProvider> */}
      <NavigationContainer>
        {
          (isLoggedIn === undefined || !isLoggedIn || isLoggedIn == null) ? (
            <Stack.Navigator>
              <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                  title: '',
                  headerShown: false
                }}
              />
            </Stack.Navigator>
          ) : (userType == '1' ? (<BottomTabAdmPus />) : (<BottomTabAdmPro />))
        }
        {/* <DisplayContent login={isLoggedIn} role={userType} /> */}
      </NavigationContainer >
      {/* </SafeAreaProvider> */}
    </AuthContext.Provider >
    // </PaperProvider>
  )
}

export default App
