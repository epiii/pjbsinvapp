import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width

const styles = StyleSheet.create({
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        color: 'blue'
    },
});
export default styles
