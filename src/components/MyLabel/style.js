import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width

const styles = StyleSheet.create({
    labelVw: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: .7 * wScreen,
        // padding: .7 * wScreen,

    },
    labelTxt: {
        width: '100%',
        // padding: .006 * wScreen,
        color: 'white',
        textAlign: 'center'
    },
});
export default styles
