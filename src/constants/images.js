// const path = '../../assets/images/'

const path = '../assets/'
const img = path + 'images/'
const icon = path + 'icons/'
const upl = path + 'uploads/'

const images = {
  // icons
  KUNCI_PAS: require("../assets/icons/kunci_pas.png"),
  PETIR: path + 'petir.png',
  PEMBANGKIT: path + 'pembangkit.png',

  // images
  CHART_HISTOGRAM: require(img + "chart_histogram.png"),
  CHART_TOOL_DIKALIBRASI: require(img + "chart_tool_dikalibrasi.png"),
  CHART_TOOL_DIPINJAM: require(img + "chart_tool_dipinjam.png"),
  CHART_CIRCLE: require(img + "chart_circle.png"),
  PETIR: require(img + "petir.png"),
  PEMBANGKIT: require(img + "pembangkit.png"),

  // uploads
  TOOL_1: require(upl + "tool_1.png"),
  TOOL_2: require(upl + "tool_2.png"),
  TOOL_3: require(upl + "tool_3.png"),
  TOOL_4: require(upl + "tool_4.png"),
  TOOL_5_HORIZ: require(upl + "tool_5_horiz.png"),
};

export default images;