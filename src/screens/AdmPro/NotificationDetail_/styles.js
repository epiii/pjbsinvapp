import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        // padding:20,
        backgroundColor: '#0B2E6A',
        // height: 2000,
    },
    bgImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    txtKeterangan: {
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 20,

    },
    dataTable: {
        marginTop: 5,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    listSubheader: {
        color: 'white'
    },
    cardDetail: {
        marginTop: 5,
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 20
    },
    notifTitle: {
        fontWeight: 'bold',
        fontSize: .05 * wScreen,
    },
    notifDesc: {
        textAlign: 'justify',
        marginTop: .04 * wScreen,
        fontSize: .035 * wScreen,
    },
    fieldArea: {
        marginTop: 20
    },
    txtHeadListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        fontSize: 15,
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold'
    },
    btnListRencanaPro: {
        marginTop: 20,
        marginBottom: 400,
        backgroundColor: '#FFAC30',
        borderRadius: 20,
        color: 'white',
        margin: 5,
        // padding: -5
    },
    txtListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        textAlign: 'center',
        color: 'black',
        // fontWeight: 'bold',
        // margin:-5,
        // height:10
    },
    vwListRencanaPro: {
        flexDirection: 'row'
    },

    cardListRencanaPro: {
        flex: 1,
        justifyContent: 'space-between',
        width: '80%',
        padding: 10,
        backgroundColor: 'white',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    iconImg: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center'
    },
    overlayImage: {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        justifyContent: "center",
        alignItems: "center",
    },
    vwStatus: {
        flex: 1,
        backgroundColor: '#FFAC30',
        // marginRight: 20,
        padding: 5,
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    cardContent: {
        padding: 10,
        marginTop: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        // flexDirection: 'column'
    },
    listRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 5,
    },
    listField: {
        textAlign: 'right',
        color: 'black',
        alignItems: 'center',
        alignSelf: 'center',
        fontSize: .04 * wScreen,
    },
    listLabel: {
        color: '#EC3247',
        fontWeight: 'bold',
        fontSize: .05 * wScreen,
        alignItems: 'center',
        alignSelf: 'center',
    },
    txtStatus: {
        color: 'white',
        textAlign: 'right'
    },
    txtLabel: {
        fontSize: 20, fontWeight: 'bold', color: 'white', textAlign: 'left'
    },
    content: {
        width: '100%',
        position: "absolute",
        marginBottom: 300,
        paddingBottom: 100,
        paddingTop: 20,
        // height:4000,
        // top: 0,
        // right: 0,
        // bottom: 0,
        // left: 0,
        justifyContent: "center",
        // alignItems: "center",
    },
    scrollView: {
        flex: 1
        // height: 3 * wScreen,
    },
    bigBlue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30,
    },
    mainText: {
        color: 'white'
    }
    // red: {
    //     color: 'red',
    // },
});
export default styles