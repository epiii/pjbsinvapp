import React, { useState, useContext, useEffect } from 'react'
import {
    Dimensions,
    TextInput,
    View,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native'
import styles from './style'
import IconFA from 'react-native-vector-icons/FontAwesome'
// import { CounterContext } from '../../providers/CounterContext'
// import { TouchableOpacity } from 'react-native-gesture-handler'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const component = ({ name, textAlign, value, val, placeholder }) => {
    const [valuex, setValuex] = useState('0');
    // const [valuex, setValuex] = useContext(CounterContext);

    useEffect(() => {
        console.log(valuex, val)

        // setValuex(value)
    }, [])

    const increment = () => {
        console.log('bef', valuex)
        setValuex((parseInt(valuex) + 1).toString())
        console.log('after', valuex)
    }

    const decrement = () => {
        console.log('bef', valuex)
        setValuex((parseInt(valuex) > 0 ? parseInt(valuex) - 1 : 0).toString())
        console.log('after', valuex)
    }

    const ButtonNum = ({ type }) => (
        <TouchableHighlight
            underlayColor={'#5CC652'}
            style={{
                flex: 3,
                paddingTop: .03 * wScreen,
                paddingHorizontal: .01 * wScreen,
                alignSelf: 'center',
                justifyContent: 'center',
                alignItems: 'center',
                // backgroundColor: 'yellow',
            }}
            onPress={() => (type == 'increment' ? increment() : decrement())}
        >
            <IconFA
                name={type == 'increment' ? 'plus' : 'minus'}
                style={{
                    flex: 1,
                }}
                size={.05 * wScreen}
                color={'#0B2E6A'}
            />
        </TouchableHighlight>
    )

    return (
        <View style={{
            flexDirection: 'row',
            backgroundColor: 'white',
            alignItems: 'center',
            justifyContent: 'space-between',
            borderRadius: .03 * wScreen,
            width: '50%'

        }}>
            <ButtonNum type={'decrement'} />
            <TextInput
                style={[styles.txtInput, {
                    // height: .1 * wScreen,
                    textAlign: textAlign ? textAlign : 'left',
                    width: '70%',
                    fontSize: .05 * wScreen,
                    fontWeight: 'bold',
                    // backgroundColor: 'red'
                    flex: 3
                }]
                }
                keyboardType={'numeric'}
                name={name}
                value={valuex}
                onChangeText={(text) => setValuex(text)}
                placeholder={placeholder}
                placeholderTextColor="grey"
            />
            <ButtonNum type={'increment'} />
            {/* <ButtonNum onPress={() => decrement()} /> */}
        </View>
    )
}


export default component
