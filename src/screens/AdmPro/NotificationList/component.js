import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button, FAB, List, DataTable } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)
const dataNotification_dummy = [
    {
        id: 1,
        notifTitle: 'Reminder Pengembalin AP-999',
        hasRead: false,
        category: 'info',
        status: null,
        date: '2020-07-10',
        notifDesc: 'desk approval proj CCC di-direject  oleh Mr. XXX jkfljskldjkjfkl lkjlksjdifusd sodufo psodi spodifod laksl;d alsdkhjdahsdjh ajsdhakjshdkajshdjahskdj',
    },
    {
        id: 2,
        notifTitle: 'Approval Proj AAA',
        category: 'approval',
        status: 'approved',
        date: '2020-05-07',
        hasRead: false,
        notifDesc: 'desk approval proj di-approve AAA di oleh Mr. XXX jkfljskldjkjfkl lkjlksjdifusd sodufo psodi spodifod laksl;d alsdkhjdahsdjh ajsdhakjshdkajshdjahskdj',
    },
    {
        id: 3,
        notifTitle: 'Approval Proj BBB',
        category: 'approval',
        date: '2020-05-17',
        status: 'reject',
        hasRead: false,
        notifDesc: 'desk approval proj BBB di-reject oleh Mr. XXX jkfljskldjkjfkl lkjlksjdifusd sodufo psodi spodifod laksl;d alsdkhjdahsdjh ajsdhakjshdkajshdjahskdj',
    },
    {
        id: 4,
        notifTitle: 'Approval Proj CCC',
        category: 'reject',
        date: '2020-07-10',
        hasRead: true,
        status: 'empty',
        notifDesc: 'desk approval proj CCC di-direject  oleh Mr. XXX jkfljskldjkjfkl lkjlksjdifusd sodufo psodi spodifod laksl;d alsdkhjdahsdjh ajsdhakjshdkajshdjahskdj',
    },
    {
        id: 5,
        notifTitle: 'Pengajuan Tool EEE',
        category: 'approval',
        status: 'approved',
        date: '2020-05-07',
        hasRead: true,
        notifDesc: 'desk approval proj di-approve AAA di oleh Mr. XXX jkfljskldjkjfkl lkjlksjdifusd sodufo psodi spodifod laksl;d alsdkhjdahsdjh ajsdhakjshdkajshdjahskdj',
    },
];

const MyList = ({ data }) => {
    const navigation = useNavigation();
    return (
        <>{
            data.map((item, index) => (
                <List.Section key={index} >
                    {/* {<List.Subheader style={styles.listSubheader}>Pengajuan Tool</List.Subheader>} */}
                    <TouchableOpacity
                        onPress={() => navigation.navigate('NotificationDetail', {
                            id: item.id,
                            notifTitle: item.notifTitle,
                            notifDesc: item.notifDesc,
                        })}
                    >
                        <List.Item
                            style={[styles.list, {
                                backgroundColor: item.hasRead ? '#C4C4C4' : 'white',
                            }]}
                            title={item.notifTitle}
                            description={item.notifDesc}
                            left={props => <List.Icon
                                {...props}
                                color={item.hasRead ? 'grey' : (item.category == 'info' ? '#1DA0F9' : (item.status == 'approved' ? "#5CC652" : '#EC3247'))}
                                icon={item.category == 'info' ? 'bell' : (item.status == 'approved' ? "check-circle" : "cancel")}
                            />}
                        />
                    </TouchableOpacity>
                </List.Section>
            ))
        }
        </>
    )
};


function Component({ navigation }) {
    const [dataNotification, setDataNotification] = useState([])

    useEffect(() => {
        setDataNotification(dataNotification_dummy)
        return () => {
            // cleanup
        }
    }, [dataNotification])

    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >
                            <MyList data={dataNotification} />
                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
