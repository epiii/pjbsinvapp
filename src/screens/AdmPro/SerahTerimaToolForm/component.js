import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInput, ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { Button, FAB, DataTable } from 'react-native-paper';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const CardToolProy = ({ tahun, data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>
            <View style={styles.toolProyTopCard}>
                <Text style={styles.txtHeadListRencanaPro}>
                    Status Tools Proyek {tahun}
                </Text>
            </View>

            <View style={styles.toolProyContentCard}>
                {data.map((item, key) => (
                    <TouchableOpacity
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                            status: item.status,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {key + 1}. {item.nama}
                            </Text>
                            <Label fgColor={'white'} bgColor={'#EC3247'} text={item.status.toUpperCase()} />
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

const CardToolDikirim = ({ data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>

            <View style={styles.toolProyContentCard}>
                {data.map((item, index) => (
                    <View style={styles.vwListRencanaPro}>
                        <Text style={styles.txtListRencanaPro}>
                            {item.nama}
                        </Text>
                        <Text style={[styles.txtListRencanaPro, { fontWeight: 'normal', color: 'black' }]}>
                            {item.deskripsi}
                        </Text>
                    </View>
                ))}
            </View>
        </View>
    )
}

const ChartProyek_dummy = ({ total }) => (
    <Image
        style={[styles.iconImg, styles.chartProySize]}
        source={IMAGES.CHART_CIRCLE}
    // source={require("../../assets/images/chart_circle.png")}
    />
)

const ChartHoriz_dummy = ({ onPress, img, keterangan, jumlah }) => (
    <TouchableHighlight underlayColor={'#043353'} onPress={onPress}>
        <View style={styles.chartHorizVw}>
            <Image
                style={{ width: '100%' }}
                source={img}
            />
            <Text style={styles.chartHorizTxt}>{keterangan}</Text>
            {/* <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{jumlah}</Text> */}
        </View>
    </TouchableHighlight>
)

const ChartTool_dummy = ({ dipinjam, dikembalikan }) => (
    <View style={styles.chartRenRealVw}>
        <Image
            style={styles.chartRenRealImg}
            source={IMAGES.CHART_HISTOGRAM}
        />
        <View style={styles.chartRenRealRow}>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'left' }]}>{'Jumlah Tools\nDipinjam'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{dipinjam} </Text>
            </View>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'right' }]}>{'Jumlah Tools\nDikembalikan'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'right' }]}>{dikembalikan} </Text>
            </View>
        </View>
    </View>
)

const ChartProyek = ({ total }) => (
    <ProgressChart
        hideLegend={true}
        data={{
            labels: ["Andi",],
            data: [0.5]
        }}
        width={0.7 * wScreen} // from react-native
        height={.4 * wScreen}
        // height={220}
        chartConfig={{
            // backgroundGradientFrom: '#0B2E6A',
            // backgroundGradientTo: 'purple',
            // color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0.0,
            color: (opacity = 1) => `rgba(236, 50, 71, ${opacity})`,
            strokeWidth: 1, // optional, default 3
        }}
    // backgroundColor="green"
    // paddingLeft="15"
    // absolute
    // style={{
    //     marginVertical: 8,
    //     borderRadius: 16
    // }}
    />
)

const CardProyek = ({ total }) => (
    <View style={styles.proyCard}>
        <Text style={styles.proyTxt}>
            Total Proyek
        </Text>
        <View style={styles.proyRow}>
            <Text style={styles.proyTotTxt}>{total}</Text>
            <Image
                style={[styles.iconImg, styles.proyIconSize]}
                source={IMAGES.KUNCI_PAS}
            // source={require("../../assets/icons/kunci_pas.png")}
            />
        </View>
    </View>
)
const dataToolProy_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
];

const MyLabel = ({ text, color }) => (
    <View style={[styles.labelVw, { backgroundColor: color }]}>
        <Text style={styles.labelTxt}>{text}</Text>
    </View>
)



const MyText = ({ text, fgColor, bgColor, textAlign, isBold }) => (
    <View>
        <Text style={{
            fontWeight: isBold ? 'bold' : 'normal',
            fontSize: isBold ? (.042 * wScreen) : (.037 * wScreen),
            color: fgColor ? fgColor : 'white',
            textAlign: textAlign ? textAlign : 'justify',
            backgroundColor: bgColor ? bgColor : 'transparent'
        }}>{text}</Text>
    </View>
)

const MyButton = ({ props, text, fgColor, bgColor, onPress }) => {
    const navigation = useNavigation();

    return (
        <Button
            {...props}
            compact={true}
            color={fgColor}
            style={[
                styles.myButton,
                {
                    alignSelf: 'flex-start',
                    backgroundColor: bgColor
                }]}
            onPress={onPress}
        > {text}</Button >
    )
}

const MyTextInput = ({ value, name, placeholder, label }) => (
    <TextInput
        style={styles.txtInput}
        name={name}
        value={value}
        placeholder={placeholder}
        placeholderTextColor="grey"
    />
)
const dataToolDikirim_dummy = [
    { id: 1, nama: 'Start Proyek', deskripsi: 'DD/MM/YYY', status: 'baru' },
    { id: 2, nama: 'Selesai Proyek', deskripsi: 'DD/MM/YYY', status: 'baru' },
]

const MyFieldVert = ({ topComponent, bottomComponent }) => (
    <View style={styles.fieldAreaVert} >
        <View style={styles.topComponent}>
            {topComponent ? topComponent : ''}
        </View>
        <View style={styles.bottomComponent}>
            {bottomComponent ? bottomComponent : ''}
        </View>
    </View>
)

const MyFieldHoriz = ({ leftComponent, rightComponent, leftFlex, rightFlex }) => (
    <View style={styles.fieldAreaHoriz} >
        <View style={[styles.leftComponent, { flex: leftFlex ? leftFlex : 1 }]}>
            {leftComponent ? leftComponent : ''}
        </View>
        <View style={[styles.rightComponent, { flex: rightFlex ? rightFlex : 1 }]}>
            {rightComponent ? rightComponent : ''}
        </View>
    </View>
)

const MyTable = () => (
    <View style={styles.tableArea}>
        <Text style={styles.tableLabel}>Tools yang dipinjam</Text>
        <DataTable style={styles.dataTable}>
            <DataTable.Header>
                <DataTable.Title>No Inv</DataTable.Title>
                <DataTable.Title >Tool</DataTable.Title>
                <DataTable.Title numeric>Qty</DataTable.Title>
                <DataTable.Title >Utilisasi</DataTable.Title>
            </DataTable.Header>

            <DataTable.Row>
                <DataTable.Cell>Mrs BBB</DataTable.Cell>
                <DataTable.Cell >Beko Y</DataTable.Cell>
                <DataTable.Cell numeric>100 </DataTable.Cell>
                <DataTable.Cell >xxxx </DataTable.Cell>
                {/* <DataTable.Cell ><MyLabel color={'#5CC652'} text={'stand by'} /></DataTable.Cell> */}
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR ZZZ</DataTable.Cell>
                <DataTable.Cell >Traktor</DataTable.Cell>
                <DataTable.Cell numeric>5</DataTable.Cell>
                <DataTable.Cell >xxxx </DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR ZZZ</DataTable.Cell>
                <DataTable.Cell >Traktor</DataTable.Cell>
                <DataTable.Cell numeric>5</DataTable.Cell>
                <DataTable.Cell >xxxx </DataTable.Cell>
            </DataTable.Row>

            <DataTable.Pagination
                page={1}
                numberOfPages={3}
                onPageChange={page => {
                    console.log(page);
                }}
                label="1-3 of 6"
            />
        </DataTable>
    </View>
);

function Component({ navigation }) {
    const [dataToolProy, setDataToolProy] = useState([])
    const [dataToolDikirim, setDataToolDikirim] = useState([])

    useEffect(() => {
        setDataToolProy(dataToolProy_dummy)
        setDataToolDikirim(dataToolDikirim_dummy)
        return () => {
        }
    }, [dataToolProy, dataToolDikirim])

    return (
        <ImageBackground
            // source={IMAGES.PETIR}
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >
                            <MyFieldHoriz
                                rightComponent={
                                    <MyButton
                                        fgColor={'white'}
                                        bgColor={'#EC3247'}
                                        onPress={() => alert('scanning QR ...')}
                                        text={'Scan QR'}
                                    />
                                }
                                leftFlex={4}
                                rightFlex={3}
                                leftComponent={<MyText fgColor={'white'} text={'No. Order'} />}
                            />
                            <MyFieldHoriz
                                leftFlex={4}
                                rightFlex={3}
                                leftComponent={<MyText fgColor={'white'} text={''} />}
                                rightComponent={<MyText fgColor={'white'} textAlign={'center'} text={'Atau'} />}
                            />
                            <MyFieldHoriz
                                leftFlex={4}
                                rightFlex={3}
                                rightComponent={
                                    <MyTextInput name={'noOrder'} placeholder={'INPUT DISINI'} />
                                }
                                leftComponent={
                                    <MyText fgColor={'white'} text={''} />
                                }
                            />
                            <MyFieldHoriz
                                leftFlex={4}
                                rightFlex={3}
                                rightComponent={
                                    <MyButton
                                        fgColor={'white'}
                                        bgColor={'#FFAC30'}
                                        onPress={() => alert('paket X')}
                                        text={'Paket X'}
                                    />
                                }
                                leftComponent={
                                    <MyText fgColor={'white'} text={'Kebutuhan Paket Tools'} />
                                }
                            />
                            <CardToolDikirim data={dataToolDikirim} />

                            <MyTable />
                            <MyFieldVert
                                topComponent={
                                    <MyText fgColor={'white'} isBold={true} text={'Keterangan'} />
                                }
                                bottomComponent={
                                    <MyText fgColor={'white'} text={'ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan ini adalah keterangan '} />
                                }
                            />
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Button
                                    color={'white'}
                                    style={{
                                        flex: 1,
                                        backgroundColor: '#FFAC30',
                                        // alignSelf: 'center',
                                        borderRadius: .06 * wScreen,
                                        paddingVertical: .01 * wScreen,
                                        marginHorizontal: .02 * wScreen,
                                    }}
                                    onPress={() => alert('lampiran...')}
                                >Lampiran</Button>
                                <Button
                                    color={'white'}
                                    style={{
                                        flex: 1,
                                        marginHorizontal: .02 * wScreen,
                                        backgroundColor: '#5CC652',
                                        // alignSelf: 'center',
                                        borderRadius: .06 * wScreen,
                                        paddingVertical: .01 * wScreen,
                                    }}
                                    onPress={() => alert('lampiran...')}
                                >Terima</Button>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground >
    )
}

export default Component
