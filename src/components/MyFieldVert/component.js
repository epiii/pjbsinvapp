import React from 'react'
import { View } from 'react-native'
import styles from './style'

const component = ({ topComponent, bottomComponent, topAlign, bottomAlign }) => (
    <View style={styles.fieldAreaVert} >
        <View style={[styles.topComponent, {
            // backgroundColor: 'yellow',
            justifyContent: topAlign ? topAlign : 'flex-start'
        }]}>
            {topComponent ? topComponent : ''}
        </View>
        <View style={[styles.bottomComponent, {
            justifyContent: bottomAlign ? bottomAlign : 'flex-start'
        }]}>
            {bottomComponent ? bottomComponent : ''}
        </View>
    </View>
)

export default component
