import { Dimensions, StyleSheet } from 'react-native';
const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0B2E6A'
    },
    bgImage: {
        flex: 1,
        resizeMode: 'cover', // or 'stretch'
    },
    txtHeadListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        fontSize: 15,
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold'
    },
    btnListRencanaPro: {
        backgroundColor: '#EC3247',
        borderRadius: 20,
        color: 'white',
        margin: 5,
        padding: -5
    },
    txtListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        textAlign: 'center',
        color: 'black',
        // fontWeight: 'bold',
        // margin:-5,
        // height:10
    },
    vwListRencanaPro: {
        flexDirection: 'row'
    },

    cardListRencanaPro: {
        flex: 1,
        justifyContent: 'space-between',
        width: '80%',
        padding: 10,
        backgroundColor: 'white',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    clrGreen: {
        backgroundColor: '#5CC652'
    },
    clrGrey: {
        backgroundColor: '#DADADA'
    },
    clrOrange: {
        backgroundColor: '#FFAC30'
    },
    clrPink: {
        backgroundColor: '#E840B3'
    },
    iconMenu: {
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        marginTop: 15,
    },
    txtMenu: {
        fontSize: .03 * wScreen,
        textAlign: 'center',
        padding: 10,
        color: 'white',
        // color: '#7C4571',
        fontWeight: 'bold'
    },
    cardMenu: {
        marginHorizontal: 5,
        height: .2 * wScreen,
        width: .2 * wScreen,
        borderRadius: 20,
        flexDirection: 'column',
        justifyContent: 'space-around',
        // opacity: .4,
        // backgroundColor: '#FFAC30'
        // backgroundColor: '#E840B3'
    },
    rowMenu: {
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    overlayImage: {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        justifyContent: "center",
        alignItems: "center",
    },
    content: {
        marginTop: 20,
        position: "absolute",
        // top: 0,
        // right: 0,
        // bottom: 0,
        // left: 0,
        justifyContent: "center",
        alignItems: "center",
    },
    bigBlue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30,
    },
    mainText: {
        color: 'white'
    }
    // red: {
    //     color: 'red',
    // },
});
export default styles