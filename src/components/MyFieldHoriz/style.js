import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width

const styles = StyleSheet.create({
    fieldAreaHoriz: {
        // backgroundColor: 'yellow',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginVertical: .01 * wScreen,
    },
    leftComponent: {
        // flex: 4,
    },
    rightComponent: {
        // flex: 3
    },

});
export default styles
