import React from 'react'
import { Text } from 'react-native'

// home 
import LoginScreen from '../screens/Login'
import DashboardAdmProScreen from '../screens/AdmPro/Dashboard'
import DashboardAdmPusScreen from '../screens/AdmPus/Dashboard'
import DetailProyekScreen from '../screens/AdmPro/DetailProyek'
import CekToolDikirimScreen from '../screens/AdmPro/CekToolDikirim'
import SerahTerimaToolFormScreen from '../screens/AdmPro/SerahTerimaToolForm'
import PersiapanToolFormScreen from '../screens/AdmPro/PersiapanToolForm'
import JWaktuPinjamToolFormScreen from '../screens/AdmPro/JWaktuPinjamToolForm'

// notification 
import NotificationListScreen from '../screens/AdmPro/NotificationList'
import NotificationDetailScreen from '../screens/AdmPro/NotificationDetail'

// tools
import ToolListScreen from '../screens/AdmPus/ToolList'

// history
import HistoryListScreen from '../screens/AdmPro/HistoryList'
import HistoryUserScreen from '../screens/AdmPro/HistoryUser'
import HistoryTransToolPusatScreen from '../screens/AdmPus/HistoryTransToolPusat'
import HistoryTransToolProyekScreen from '../screens/AdmPro/HistoryTransToolProyek'

// add
import AddListScreen from '../screens/AdmPro/AddList'
import PengembalianToolPusatFormScreen from '../screens/AdmPro/PengembalianToolPusatForm'
import PengembalianToolProyekFormScreen from '../screens/AdmPro/PengembalianToolProyekForm'
import PeminjamanToolProyekFormScreen from '../screens/AdmPro/PeminjamanToolProyekForm'
import PeminjamanToolPusatFormScreen from '../screens/AdmPro/PeminjamanToolPusatForm'

// more
import MoreListScreen from '../screens/AdmPro/MoreList'
import StokGudangScreen from '../screens/AdmPro/StokGudang'
import DetailKalibrasiScreen from '../screens/AdmPro/DetailKalibrasi'
import ReportGapToolScreen from '../screens/AdmPro/ReportGapTool'

// account
import AccountScreen from '../screens/AdmPro/Account'
import ProfileScreen from '../screens/Profile'

import RightHeader from '../components/RightHeader'

import { createStackNavigator } from '@react-navigation/stack'

const RootStack = createStackNavigator()

// pro
const HistoryAdmProStack = createStackNavigator()
const HomeAdmProStack = createStackNavigator()
const HomeAdmPusStack = createStackNavigator()
const NotificationStack = createStackNavigator()
const HistoryStack = createStackNavigator()
const MoreStack = createStackNavigator()
const AccountStack = createStackNavigator()
const AddStack = createStackNavigator()

//pus 
const ToolStack = createStackNavigator()

const TabScreenOptions = {
    headerStyle: {
        backgroundColor: '#0B2E6A'
    },
    headerTintColor: 'white',
    headerTitleStyle: {
        fontWeight: 'bold'
    },
    // headerRight: () => (
    //     <RightHeader
    //         icon={'user'}
    //         fgColor={'white'}
    //         bgColor={'red'}
    //         onPress={() => alert('user')}
    //     />
    // )
}

const RootStackScreen = () => (
    <RootStack.Navigator >
        {/* auth */}
        {/* <RootStack.Screen
            name="Auth"
            component={AuthStackScreen}
        /> */}

        {/* Admin Proyek */}
        {/* <RootStack.Screen
            name="Home"
            component={HomeAdmProStackScreen}
        /> */}
        {/*
        <RootStack.Screen
            name="AdmProTool"
            component={ToolStackScreen}
        />
        <RootStack.Screen
            name="AdmProNotification"
            component={NotificationStackScreen}
        />
        <RootStack.Screen
            name="AdmProMore"
            component={MoreStackScreen}
        /> */}

        {/* Admin Pusat */}
        {/* todo  */}
    </RootStack.Navigator >
)

const HistoryStackScreen = () => (
    <HistoryStack.Navigator screenOptions={TabScreenOptions}>
        <HistoryStack.Screen
            name="HistoryList"
            component={HistoryListScreen}
            options={{
                title: 'History List',
            }}
        />
        <HistoryStack.Screen
            name="HistoryTransToolProyek"
            component={HistoryTransToolProyekScreen}
            options={{
                title: 'History Transaksi Tools Proyek',
            }}
        />
        <HistoryStack.Screen
            name="HistoryUser"
            component={HistoryUserScreen}
            options={{
                title: 'History User Tools',
            }}
        />
        <HistoryStack.Screen
            name="PersiapanToolForm"
            component={PersiapanToolFormScreen}
            options={{
                title: 'Persiapan Tool Proyek',
            }}
        />
        <HistoryStack.Screen
            name="JWaktuPinjamToolForm"
            component={JWaktuPinjamToolFormScreen}
            options={{
                title: 'Jangka Waktu Pinjam Tools',
            }}
        />
    </HistoryStack.Navigator >
)
// const TabRightButton = {
//     headerRight: () => (
//         <RightHeader
//             icon={'user'}
//             fgColor={'white'}
//             bgColor={'red'}
//             onPress={() => navigation.navigate('Account')}
//         />
//     )
// }

const HomeAdmProStackScreen = () => (
    <HomeAdmProStack.Navigator
        screenOptions={{
            headerStyle: {
                backgroundColor: '#0B2E6A'
            },
            headerTintColor: 'white',
            headerTitleStyle: {
                fontWeight: 'bold'
            },
            headerRight: () => (
                <RightHeader
                    icon={'user'}
                    fgColor={'white'}
                    bgColor={''}
                    onPress={'CekToolDikirim'}
                // onPress={() => useNavigation().navigate('Account')}
                // onPress={() => navigation.navigate('Account')}
                />
            )
        }}>
        {/* <HomeAdmProStack.Screen
            name="LoginForm"
            component={LoginScreen}
            options={{
                title: 'Login',
            }}
        /> */}

        <HomeAdmProStack.Screen
            name="Home"
            // component={PeminjamanToolFormScreen}
            component={DashboardAdmProScreen}
            options={{
                title: 'Dashboard Admin Proyek',
            }}
        />
        <HomeAdmProStack.Screen
            name="DetailProyek"
            component={DetailProyekScreen}
            options={{
                title: 'Detail Proyek',
            }}
        />
        <HomeAdmProStack.Screen
            name="CekToolDikirim"
            component={CekToolDikirimScreen}
            options={{
                title: 'Persiapan Tools Proyek AAA',
            }}
        />
        <HomeAdmProStack.Screen
            name="SerahTerimaToolForm"
            component={SerahTerimaToolFormScreen}
            options={{
                title: 'Form Serah Terima Tools',
            }}
        />
        <HomeAdmProStack.Screen
            name="PersiapanToolForm"
            component={PersiapanToolFormScreen}
            options={{
                title: 'Persiapan Tool Proyek',
            }}
        />
        <HomeAdmProStack.Screen
            name="JWaktuPinjamToolForm"
            component={JWaktuPinjamToolFormScreen}
            options={{
                title: 'Jangka Waktu Pinjam Tools',
            }}
        />
    </HomeAdmProStack.Navigator >
)

const HomeAdmPusStackScreen = () => (
    <HomeAdmPusStack.Navigator screenOptions={TabScreenOptions}>
        <HomeAdmPusStack.Screen
            name="Home"
            component={DashboardAdmPusScreen}
            options={{
                title: 'Dashboard Admin Pusat',
            }}
        />
        <HomeAdmPusStack.Screen
            name="DetailProyek"
            component={DetailProyekScreen}
            options={{
                title: 'Detail Proyek',
            }}
        />
        <HomeAdmPusStack.Screen
            name="PersiapanToolForm"
            component={PersiapanToolFormScreen}
            options={{
                title: 'Persiapan Tool Proyek',
            }}
        />
        <HomeAdmPusStack.Screen
            name="JWaktuPinjamToolForm"
            component={JWaktuPinjamToolFormScreen}
            options={{
                title: 'Jangka Waktu Pinjam Tools',
            }}
        />
    </HomeAdmPusStack.Navigator >
)

const ToolStackScreen = () => (
    <ToolStack.Navigator screenOptions={TabScreenOptions}>
        <ToolStack.Screen
            name="ToolList"
            component={ToolListScreen}
            options={{
                title: 'Tool',
            }}
        />
        <ToolStack.Screen
            name="HistoryTransToolPusat"
            component={HistoryTransToolPusatScreen}
            options={{
                title: 'History Transaksi Tool Pusat',
            }}
        />
    </ToolStack.Navigator >
)

const AddStackScreen = () => (
    <AddStack.Navigator screenOptions={TabScreenOptions}>
        <AddStack.Screen
            name="AddList"
            component={AddListScreen}
            options={{
                title: 'Add Menu',
            }}
        />
        <AddStack.Screen
            name="PeminjamanToolProyekForm"
            component={PeminjamanToolProyekFormScreen}
            options={{
                title: 'Form Peminjaman Tools Proyek',
            }}
        />
        <AddStack.Screen
            name="PeminjamanToolPusatForm"
            component={PeminjamanToolPusatFormScreen}
            options={{
                title: 'Form Permintaan Tools ke Pusat',
            }}
        />
        <AddStack.Screen
            name="PengembalianToolPusatForm"
            component={PengembalianToolPusatFormScreen}
            options={{
                title: 'Form Pengembalian Tools ke Pusat',
            }}
        />
        <AddStack.Screen
            name="PengembalianToolProyekForm"
            component={PengembalianToolProyekFormScreen}
            options={{
                title: 'Form Pengembalian Tools Proyek',
            }}
        />
    </AddStack.Navigator >
)

const MoreStackScreen = () => (
    <MoreStack.Navigator screenOptions={TabScreenOptions}>
        <MoreStack.Screen
            name="MoreList"
            component={MoreListScreen}
            options={{
                title: 'More Menu',
            }}
        />
        <MoreStack.Screen
            name="StokGudang"
            component={StokGudangScreen}
            options={{
                title: 'Stok Gudang',
            }}
        />
        <MoreStack.Screen
            name="DetailKalibrasi"
            component={DetailKalibrasiScreen}
            options={{
                title: 'Detail Kalibrasi',
            }}
        />
        <MoreStack.Screen
            name="ReportGapTool"
            component={ReportGapToolScreen}
            options={{
                title: 'Report Gap Tool',
            }}
        />
    </MoreStack.Navigator >
)

const AccountStackScreen = () => (
    <AccountStack.Navigator
        // initialRouteName={LoginScreen}
        screenOptions={TabScreenOptions}
    >
        <AccountStack.Screen
            name="Profile"
            component={ProfileScreen}
            options={{
                title: 'Profile',
            }}
        />
        <AccountStack.Screen
            name="Login"
            component={LoginScreen}
            options={{
                title: 'Login',
            }}
        />
        <AccountStack.Screen
            name="SignUp"
            component={SignUpScreen}
            options={{
                title: 'Sign Up',
            }}
        />
        {/* 
        <AccountStack.Screen
            name="Account"
            component={LoginScreen}
            options={{
                title: 'Account',
            }}
        /> */}
    </AccountStack.Navigator >
)

const NotificationStackScreen = () => (
    <NotificationStack.Navigator mode={'modal'} screenOptions={TabScreenOptions}>
        <NotificationStack.Screen
            name="NotificationList"
            component={NotificationListScreen}
            options={{
                title: 'Notification List',
            }}
        />
        <NotificationStack.Screen
            name="NotificationDetail"
            component={NotificationDetailScreen}
            options={{
                title: 'Notification Detail',
            }}
        />
    </NotificationStack.Navigator >
)

// const AuthStackScreen = () => (
//     <AuthStack.Navigator screenOptions={TabScreenOptions}>
//         <AuthStack.Screen
//             name="Profile"
//             component={ProfileScreen}
//             options={{
//                 title: 'Profile',
//             }}
//         />
//         <AuthStack.Screen
//             name="Login"
//             component={LoginScreen}
//             options={{
//                 title: 'Login',
//             }}
//         />
//         <AuthStack.Screen
//             name="SignUp"
//             component={SignUpScreen}
//             options={{
//                 title: 'Sign Up',
//             }}
//         />
//     </AuthStack.Navigator >
// )

export {
    // RootStackScreen,
    HomeAdmProStackScreen,
    // HistoryAdmProStackScreen,
    HistoryStackScreen,
    HomeAdmPusStackScreen,
    ToolStackScreen,
    MoreStackScreen,
    AddStackScreen,
    AccountStackScreen,
    // AccountScreen,
    NotificationStackScreen,
    // AccountScreen,
    // AuthStackScreen
};