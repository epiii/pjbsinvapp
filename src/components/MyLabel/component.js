import React from 'react'
import { View, Text } from 'react-native'
import styles from './style'

const component = ({ text, color }) => (
    <View style={[styles.labelVw, { backgroundColor: color }]}>
        <Text style={styles.labelTxt}>{text}</Text>
    </View>
)

export default component
