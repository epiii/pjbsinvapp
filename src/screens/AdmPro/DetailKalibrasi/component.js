import React, { useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import styles from './styles'
import {
    TouchableOpacity,
    ImageBackground,
    Dimensions,
    ScrollView,
    View,
    Text,
    Image,
    Modal,
    Alert,
    TextInput,
} from 'react-native'
import { IMAGES } from '../../../constants'
import { TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { Button, FAB, DataTable } from 'react-native-paper';
import {
    ProgressChart,
    LineChart,
} from "react-native-chart-kit";
import Icon from 'react-native-vector-icons/FontAwesome'
import MyLabel from '../../../components/MyLabel'
import MyText from '../../../components/MyText'
import MyFieldHoriz from '../../../components/MyFieldHoriz'
import MyFieldVert from '../../../components/MyFieldVert'
import MyTextInput from '../../../components/MyTextInput'
import MyNumberInput from '../../../components/MyNumberInput'
import { CounterContextProvider } from '../../../providers/CounterContext'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const Separator = ({ size }) => (
    <View style={{ height: size ? size : (.08 * wScreen) }}></View>
)

const ChartProyek_dummy = ({ total }) => (
    <Image
        style={[styles.iconImg, styles.chartProySize]}
        source={require("../../../assets/images/chart_circle.png")}
    />
)

const ChartHoriz_dummy = ({ onPress, img, keterangan, jumlah }) => (
    <TouchableHighlight underlayColor={'#043353'} onPress={onPress}>
        <View style={styles.chartHorizVw}>
            <Image
                style={{ width: '100%' }}
                source={img}
            />
            <Text style={styles.chartHorizTxt}>{keterangan}</Text>
            {/* <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{jumlah}</Text> */}
        </View>
    </TouchableHighlight>
)

const ChartRealisasi_dummy = ({ rencana, realisasi }) => (
    <View style={styles.chartRenRealVw}>
        <Image
            style={styles.chartRenRealImg}
            source={require("../../../assets/images/chart_histogram.png")}
        />
        <View style={styles.chartRenRealRow}>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'left' }]}>{'Rencana\nProyek'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{rencana} %</Text>
            </View>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'right' }]}>{'Realisasi\nProyek'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'right' }]}>{realisasi} %</Text>
            </View>
        </View>
    </View>
)

const ChartToolStock = ({ total }) => {
    const data = {
        labels: ["January", "February", "March", "April", "May", "June"],
        datasets: [
            {
                data: [20, 45, 28, 80, 99, 43],
                color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
                strokeWidth: 2 // optional
            }
        ],
        legend: ["Jumlah Tools"] // optional
    };

    const chartConfig = {
        backgroundColor: "white",
        backgroundGradientFrom: "white",
        backgroundGradientTo: "white",
        decimalPlaces: 2, // optional, defaults to 2dp
        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        style: {
            borderRadius: 30,
        },
        propsForDots: {
            r: "6",
            strokeWidth: "2",
            stroke: "#ffa726"
        }
    }

    return (
        <LineChart
            data={data}
            width={wScreen}
            height={220}
            chartConfig={chartConfig}
        />
    )
}

const ChartProyek = ({ total }) => (
    <ProgressChart
        hideLegend={true}
        data={{
            labels: ["Andi",],
            data: [0.5]
        }}
        width={0.7 * wScreen} // from react-native
        height={.4 * wScreen}
        // height={220}
        chartConfig={{
            // backgroundGradientFrom: '#0B2E6A',
            // backgroundGradientTo: 'purple',
            // color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0.0,
            color: (opacity = 1) => `rgba(236, 50, 71, ${opacity})`,
            strokeWidth: 1, // optional, default 3
        }}
    // backgroundColor="green"
    // paddingLeft="15"
    // absolute
    // style={{
    //     marginVertical: 8,
    //     borderRadius: 16
    // }}
    />
)

const CardMini = ({ name, total }) => (
    <View style={styles.proyCard}>
        <Text style={styles.proyTxt}>
            {name}
        </Text>
        <View style={styles.proyRow}>
            <Text style={styles.proyTotTxt}>{total}</Text>
        </View>
    </View>
)
const TransaksiToolTbl = () => (
    <DataTable style={styles.dataTable}>
        <DataTable.Header>
            <DataTable.Title>Proj.</DataTable.Title>
            <DataTable.Title numeric>Mulai</DataTable.Title>
            <DataTable.Title numeric>Selesai</DataTable.Title>
            <DataTable.Title numeric>Status</DataTable.Title>
        </DataTable.Header>

        <DataTable.Row>
            <DataTable.Cell numeric>Project AAA</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell ><MyLabel color={'#EC3247'} text={'Terlewat'} /></DataTable.Cell>
        </DataTable.Row>

        <DataTable.Row>
            <DataTable.Cell numeric>Project AAA</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell ><MyLabel color={'#5CC652'} text={'Aman'} /></DataTable.Cell>
        </DataTable.Row>

        <DataTable.Row>
            <DataTable.Cell numeric>Project AAA</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell ><MyLabel color={'#FFAC30'} text={'Terjadwal'} /></DataTable.Cell>
        </DataTable.Row>

        <DataTable.Pagination
            page={1}
            numberOfPages={3}
            onPageChange={page => {
                console.log(page);
            }}
            label="1-3 of 6"
        />
    </DataTable>
);

const CardImage = ({ title, item, onPress }) => {
    return (
        <TouchableOpacity
            style={{ marginVertical: .04 * wScreen }}
            onPress={() => onPress}
        >
            <MyText isBold={true} text={'Detail Kalibrasi Tools'} />
            <View style={styles.itemContainer}>
                <View style={styles.thumbOverlay}></View>
                <Image style={styles.thumbImg} source={item.img} />

                {item.isCalibrated ? (<Text style={styles.thumbCalibTxt}>{item.isCalibrated ? 'Kalibrasi' : ''}</Text>) : null}
                <Text style={styles.thumbNoTxt}>{item.inventoryNo}</Text>
                <Text style={styles.thumbNameTxt}>{item.name}</Text>
                <Text style={styles.thumbCatTxt}>{item.category}</Text>
            </View>
        </TouchableOpacity>
    );
}


function Component({ navigation }) {
    const [stokTool, setStokTool] = useState(0);
    const [ketTool, setKetTool] = useState('');
    const [modalVisible, setModalVisible] = useState(false);

    const openModalForm = (id) => {
        setModalVisible(true)
        // setStokTool()
        console.log('id', id)
    }

    const handleSaveStok = () => {
        setStokTool(20)
        setModalVisible(false)
    }

    const handleEditStok = () => {
        Alert.alert(
            'Konfirmasi ',
            'Anda yakin mengubah data ?',
            [
                {
                    text: 'Tidak',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                },
                { text: 'Ya', onPress: () => handleSaveStok() }
            ],
            { cancelable: false }
        );
    }

    return (
        <CounterContextProvider>
            <ImageBackground
                source={require("../../../assets/images/petir.png")}
                style={[styles.bgImage, {
                    backgroundColor: '#0B2E6A',
                }]}
            >
                <ImageBackground
                    source={require("../../../assets/images/pembangkit.png")}
                    style={styles.bgImage}
                >
                    <SafeAreaView style={styles.safeAreaView}>
                        <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                            <View style={{ flex: 1 }} >
                                <View style={styles.section}>
                                    <Text style={styles.sectionSub}>Jadwal Kalibrasi</Text>

                                    <TouchableOpacity onPress={() => alert('on development...')}>
                                        <View style={styles.filter}>
                                            <Text style={[styles.sectionSub, { marginRight: .02 * wScreen, color: 'black' }]}>Semua</Text>
                                            <Icon name={'chevron-down'} />
                                        </View>
                                    </TouchableOpacity>
                                </View>

                                <TransaksiToolTbl />
                                <CardImage item={{
                                    isCalibrated: true,
                                    inventoryNo: 'No. Inventaris',
                                    name: 'Nama Tool',
                                    category: 'utilisasi',
                                    img: IMAGES.TOOL_5_HORIZ,
                                }} onPress={() => alert('zoom in image')} />

                                <MyFieldHoriz
                                    leftComponent={<MyText isBold={true} text={'Lokasi saat ini :'} />}
                                    rightComponent={<MyText textAlign={'right'} isBold={true} text={'Jumlah Stok :'} />}
                                />
                                <MyFieldHoriz
                                    leftComponent={<MyText text={'Surabaya'} />}
                                    rightComponent={(
                                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }}>
                                            <MyText textAlign={'right'} text={stokTool} />
                                            <Button
                                                color={'#F2994A'}
                                                mode={'contained'}
                                                compact={true}
                                                style={{
                                                    height: .06 * wScreen,
                                                    paddingVertical: .008 * wScreen,
                                                    paddingHorizontal: .02 * wScreen,
                                                    marginLeft: .05 * wScreen
                                                }}
                                                contentStyle={{
                                                    height: .04 * wScreen,
                                                }}
                                                labelStyle={{
                                                    color: 'white', textTransform: 'capitalize',
                                                }}
                                                onPress={() => openModalForm()}>edit</Button>
                                            {/* onPress={() => alert('edit')}>edit</Button> */}
                                            {/* </View> */}
                                        </View>
                                    )}
                                />
                                <Separator />
                                <MyFieldVert
                                    topComponent={<MyText text={'Keterangan :'} isBold={true} />}
                                    bottomComponent={<MyText text={'Alat tersebut mengalami keterlambatan kalibrasi selama 7 hari 14 jam. '} />}
                                />

                                <Modal
                                    animationType="slide"
                                    transparent={true}
                                    visible={modalVisible}
                                    onRequestClose={() => {
                                        // Alert.alert("Modal has been closed.");
                                    }}
                                >
                                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >

                                        <View style={{
                                            backgroundColor: '#000000aa',
                                            flex: 1
                                        }}>
                                            <View style={styles.modalForm}>
                                                <MyText textAlign={'center'} text={'EDIT'} isBold={true} />
                                                <MyFieldVert
                                                    topComponent={<MyText isBold={true} fgColor={'white'} isBold={true} text={'Stok'} />}
                                                    bottomComponent={
                                                        <MyNumberInput textAlign={'center'} value={stokTool} val={'stokTool'} name={'stokTool'} placeholder={'Stok'} />
                                                    }
                                                />

                                                <MyFieldVert
                                                    topComponent={<MyText isBold={true} fgColor={'white'} isBold={true} text={'Keterangan'} />}
                                                    bottomComponent={
                                                        <MyTextInput value={ketTool} isMultiLine={true} name={'ketTool'} placeholder={'keterangan'} />
                                                    }
                                                />

                                                <MyFieldHoriz
                                                    leftComponent={
                                                        <Button
                                                            color={'#5CC652'}
                                                            style={[styles.modalButton, {
                                                                backgroundColor: 'white',
                                                                marginVertical: .05 * wScreen,
                                                                marginRight: .02 * wScreen,
                                                            }]}
                                                            mode={'outlined'}
                                                            onPress={() => setModalVisible(!modalVisible)}
                                                        > Batal</Button>
                                                    }
                                                    rightComponent={
                                                        <Button
                                                            color={'white'}
                                                            style={[styles.modalButton, {
                                                                marginVertical: .05 * wScreen,
                                                                marginLeft: .02 * wScreen,
                                                                backgroundColor: '#5CC652',
                                                                textTransform: 'lowerCase'
                                                            }]}
                                                            // icon="save"
                                                            onPress={() => handleEditStok()}
                                                        >Simpan</Button>
                                                    }
                                                />

                                            </View>
                                        </View>
                                    </ScrollView>
                                </Modal>


                            </View>
                        </ScrollView>
                    </SafeAreaView>

                </ImageBackground>
            </ImageBackground>
        </CounterContextProvider>
    )
}

export default Component
