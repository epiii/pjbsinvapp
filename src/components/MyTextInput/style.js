import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width

const styles = StyleSheet.create({
    txtInput: {
        // marginTop: 5,
        backgroundColor: 'white',
        borderRadius: .03 * wScreen,
        paddingHorizontal: .05 * wScreen,
        // alignItems:'stretch'
    },
});
export default styles
