import React from 'react'
import { TouchableOpacity, View, Animated, Text } from 'react-native'
import Icon from 'react-native-vector-icons/Foundation'
import styles from './style'
// import { TouchableOpacity } from 'react-native-gesture-handler'
// import Animated from 'react-native-reanimated'

const component = ({ bgColor, fgColor, icon }) => {
    return (
        <View
            style={{
                zIndex: 99,
                position: 'absolute',
                alignItems: 'center',
            }}
        >
            <Animated.View style={[styles.button, { backgroundColor: bgColor }]}>
                <TouchableOpacity onPress={() => alert('Add')}>
                    <View>
                        <Icon
                            // name="page-edit"
                            name={icon}
                            size={26}
                            color={fgColor}
                        />
                    </View>
                </TouchableOpacity>
            </Animated.View>
        </View>
    )
}

export default component
