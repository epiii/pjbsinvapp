import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInput, ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FAB } from 'react-native-paper';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const CardToolProy = ({ tahun, data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>
            <View style={styles.toolProyTopCard}>
                <Text style={styles.txtHeadListRencanaPro}>
                    Status Tools Proyek {tahun}
                </Text>
            </View>

            <View style={styles.toolProyContentCard}>
                {data.map((item, index) => (
                    <TouchableOpacity
                        key={index}
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                            status: item.status,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {index + 1}. {item.nama}
                            </Text>
                            <Label fgColor={'white'} bgColor={'#EC3247'} text={item.status.toUpperCase()} />
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

const MyFieldVert = ({ topComponent, bottomComponent }) => (
    <View style={styles.fieldAreaVert} >
        <View style={styles.topComponent}>
            {topComponent ? topComponent : ''}
        </View>
        <View style={styles.bottomComponent}>
            {bottomComponent ? bottomComponent : ''}
        </View>
    </View>
)

const MyFieldHoriz = ({ leftComponent, rightComponent, leftFlex, rightFlex }) => (
    <View style={styles.fieldAreaHoriz} >
        <View style={[styles.leftComponent, { flex: leftFlex ? leftFlex : 1 }]}>
            {leftComponent ? leftComponent : ''}
        </View>
        <View style={[styles.rightComponent, { flex: rightFlex ? rightFlex : 1 }]}>
            {rightComponent ? rightComponent : ''}
        </View>
    </View>
)


// const MyFieldHoriz = ({ value, label }) => (
//     <View style={styles.fieldAreaHoriz} >
//         <Text style={styles.fieldLabel}>{label}</Text>
//         <Label fgColor={'white'} text={value} bgColor={'#FFAC30'} />
//     </View>
// )


const MyText = ({ text, fgColor, bgColor, textAlign, isBold }) => (
    <View>
        <Text style={{
            fontWeight: isBold ? 'bold' : 'normal',
            fontSize: isBold ? (.042 * wScreen) : (.037 * wScreen),
            color: fgColor ? fgColor : 'white',
            textAlign: textAlign ? textAlign : 'justify',
            backgroundColor: bgColor ? bgColor : 'transparent'
        }}>{text}</Text>
    </View>
)
// const MyText = ({ value, label }) => (
//     <View style={styles.fieldArea} >
//         <Text style={styles.fieldLabel}>{label} :</Text>
//         <Text style={styles.fieldValue}>{value}</Text>
//     </View>
// )

const MyButton = ({ isLowerCase, isFullWidth, props, text, fgColor, bgColor, onPress }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.myButton}>

            <Button
                {...props}
                compact={true}
                color={fgColor}
                style={[styles.btnListRencanaPro, {
                    backgroundColor: bgColor,
                    // alignSelf: isFullWidth ? '' : 'center',
                    // alignSelf:''
                    // alignItems:'flex-end',
                    // alignContent:'flex-end',
                    // alignSelf:'baseline'

                }]}
                onPress={onPress}
            >
                <Text style={{
                    textTransform: isLowerCase ? 'capitalize' : 'uppercase',
                }}>
                    {text}
                </Text>
            </Button >
        </View>
    )
}

// const MyButton = () => {
//     const navigation = useNavigation();

//     return (
//         <View style={styles.myButton}>
//             <Button
//                 color='white'
//                 style={[styles.btnListRencanaPro, { backgroundColor: '#FFAC30' }]}
//                 onPress={() => navigation.navigate('CekToolDikirim')}
//             >Cek Tools</Button>
//         </View>
//     )
// }

const CardToolDikirim = ({ data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>

            <View style={styles.toolProyContentCard}>
                {data.map((item, index) => (
                    <View style={styles.vwListRencanaPro}>
                        <Text style={styles.txtListRencanaPro}>
                            {item.nama}
                        </Text>
                        <Text style={[styles.txtListRencanaPro, { fontWeight: 'normal', color: 'black' }]}>
                            {item.deskripsi}
                        </Text>
                    </View>
                ))}
            </View>
        </View>
    )
}

const ChartProyek_dummy = ({ total }) => (
    <Image
        style={[styles.iconImg, styles.chartProySize]}
        source={IMAGES.CHART_CIRCLE}
    // source={require("../../assets/images/chart_circle.png")}
    />
)

const ChartHoriz_dummy = ({ onPress, img, keterangan, jumlah }) => (
    <TouchableHighlight underlayColor={'#043353'} onPress={onPress}>
        <View style={styles.chartHorizVw}>
            <Image
                style={{ width: '100%' }}
                source={img}
            />
            <Text style={styles.chartHorizTxt}>{keterangan}</Text>
            {/* <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{jumlah}</Text> */}
        </View>
    </TouchableHighlight>
)

const ChartTool_dummy = ({ dipinjam, dikembalikan }) => (
    <View style={styles.chartRenRealVw}>
        <Image
            style={styles.chartRenRealImg}
            source={IMAGES.CHART_HISTOGRAM}
        />
        <View style={styles.chartRenRealRow}>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'left' }]}>{'Jumlah Tools\nDipinjam'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{dipinjam} </Text>
            </View>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'right' }]}>{'Jumlah Tools\nDikembalikan'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'right' }]}>{dikembalikan} </Text>
            </View>
        </View>
    </View>
)

const ChartProyek = ({ total }) => (
    <ProgressChart
        hideLegend={true}
        data={{
            labels: ["Andi",],
            data: [0.5]
        }}
        width={0.7 * wScreen} // from react-native
        height={.4 * wScreen}
        // height={220}
        chartConfig={{
            // backgroundGradientFrom: '#0B2E6A',
            // backgroundGradientTo: 'purple',
            // color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0.0,
            color: (opacity = 1) => `rgba(236, 50, 71, ${opacity})`,
            strokeWidth: 1, // optional, default 3
        }}
    // backgroundColor="green"
    // paddingLeft="15"
    // absolute
    // style={{
    //     marginVertical: 8,
    //     borderRadius: 16
    // }}
    />
)

const CardProyek = ({ total }) => (
    <View style={styles.proyCard}>
        <Text style={styles.proyTxt}>
            Total Proyek
        </Text>
        <View style={styles.proyRow}>
            <Text style={styles.proyTotTxt}>{total}</Text>
            <Image
                style={[styles.iconImg, styles.proyIconSize]}
                source={IMAGES.KUNCI_PAS}
            // source={require("../../assets/icons/kunci_pas.png")}
            />
        </View>
    </View>
)
const dataToolProy_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
];

const dataToolDikirim_dummy = [
    { id: 1, nama: 'Start Proyek', deskripsi: 'DD/MM/YYY', status: 'baru' },
    { id: 2, nama: 'Selesai Proyek', deskripsi: 'DD/MM/YYY', status: 'baru' },
]

const dataToolDikirim2_dummy = [
    { id: 1, nama: 'Customer', deskripsi: 'John Doe' },
    { id: 2, nama: 'Warehouse', deskripsi: 'Lorem ipsum' },
    { id: 3, nama: 'Tipe Proyek', deskripsi: 'Lorem ipsum' },
    { id: 4, nama: 'Manajer Proyek', deskripsi: 'Lorem ipsum' },
    { id: 5, nama: 'Rendal Proyek', deskripsi: 'Lorem ipsum' },
]

function Component({ navigation }) {
    const [dataToolProy, setDataToolProy] = useState([])
    const [dataToolDikirim, setDataToolDikirim] = useState([])
    const [dataToolDikirim2, setDataToolDikirim2] = useState([])

    useEffect(() => {
        setDataToolDikirim(dataToolDikirim_dummy)
        setDataToolDikirim2(dataToolDikirim2_dummy)
        return () => {
            // cleanup
        }
    }, [dataToolDikirim2, dataToolDikirim])

    return (
        <ImageBackground
            // source={IMAGES.PETIR}
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >
                            <MyFieldVert
                                topComponent={<MyText isBold={true} fgColor={'white'} text={'Deskripsi'} />}
                                bottomComponent={
                                    <MyText text={'ini keterangan bla bla bla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla '} />
                                }
                            />
                            <MyFieldHoriz
                                leftComponent={
                                    <MyText isBold={true} text={'Lokasi Proyek'} />
                                }
                                rightComponent={
                                    <MyText textAlign={'right'} text={'UBJOM Gresik'} />
                                }
                            />
                            <MyFieldHoriz
                                leftComponent={
                                    <MyText isBold={true} text={'Status'} />
                                }
                                leftFlex={2}
                                rightFlex={3}
                                rightComponent={
                                    <MyButton
                                        fgColor={'white'}
                                        bgColor={'#FFAC30'}
                                        onPress={() => alert('paket X')}
                                        text={'Yang Akan Datang'}
                                        isLowerCase={true}
                                    />
                                }
                            />

                            {/* <MyFieldHoriz label={'Status'} value={'Yang Akan Datang'} /> */}
                            <CardToolDikirim data={dataToolDikirim} />
                            <CardToolDikirim data={dataToolDikirim2} />

                            {/* <MyButton
                                fgColor={'white'}
                                bgColor={'#FFAC30'}
                                onPress={() => alert('Simpan....')}
                                text={'Cek Tools'}
                                isFullWidth={false}
                            /> */}

                            <Button
                                compact={true}
                                color={'white'}
                                style={[styles.btnListRencanaPro, {
                                    backgroundColor: '#FFAC30',
                                    alignSelf: 'center',
                                    marginVertical: .04 * wScreen,
                                }]}
                                onPress={() => navigation.navigate('CekToolDikirim')}
                            >
                                <Text style={{
                                    // textTransform: isLowerCase ? 'capitalize' : 'uppercase',
                                }}>
                                    cek tools
                                </Text>
                            </Button >

                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
