import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width

const styles = StyleSheet.create({
    fieldAreaVert: {
        // justifyContent: 'space-between',
        marginVertical: .02 * wScreen,
        // alignItems: 'center',
    },
    topComponent: {
        paddingBottom: .02 * wScreen
        // flex: 4,
        // backgroundColor: 'green'
    },
    bottomComponent: {
        // flex: 3
    },
});
export default styles
