import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { PieChart, ProgressChart } from 'react-native-chart-kit'
import { Button } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FAB } from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const CardToolProy = ({ tahun, data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>
            <View style={styles.toolProyTopCard}>
                <Text style={styles.txtHeadListRencanaPro}>
                    Status Tools Proyek {tahun}
                </Text>
            </View>

            <View style={styles.toolProyContentCard}>
                {data.map((item, index) => (
                    <TouchableOpacity
                        key={index}
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                            status: item.status,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {index + 1}. {item.nama}
                            </Text>
                            <Label fgColor={'white'} bgColor={item.status == 'baru' ? '#EC3247' : '#5CC652'} text={item.status.toUpperCase()} />
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

const CardToolDikirim = ({ data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>
            <View style={styles.toolProyTopCard}>
                <Text style={styles.txtHeadListRencanaPro}>
                    Tool Dikirim
                </Text>
            </View>

            <View style={styles.toolProyContentCard}>
                {data.map((item, index) => (
                    <TouchableOpacity
                        key={index}
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                            status: item.status,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {index + 1}. {item.nama}
                            </Text>
                            <Label fgColor={'white'} bgColor={'#EC3247'} text={item.status.toUpperCase()} />
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

const ChartProyek_dummy = ({ total }) => (
    <Image
        style={[styles.iconImg, styles.chartProySize]}
        source={IMAGES.CHART_CIRCLE}
    // source={require("../../assets/images/chart_circle.png")}
    />
)

const PieChartTool = ({ total }) => (
    <PieChart
        data={[
            {
                name: "Dipinjam",
                population: 100,
                color: "#CE2166",
                legendFontColor: "white",
                legendFontSize: .04 * wScreen
            },
            {
                name: "Dikembalikan",
                population: 60,
                color: "#FFAC30",
                legendFontColor: "white",
                legendFontSize: .04 * wScreen
            },
        ]}
        width={.9 * wScreen}
        height={.6 * wScreen}
        // height={220}
        chartConfig={{
            decimalPlaces: 2, // optional, defaults to 2dp
            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
            style: {
                borderRadius: 16
            },
            // propsForDots: {
            //     r: "6",
            //     strokeWidth: "2",
            //     stroke: "#ffa726"
            // }
        }}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="15"
        absolute
    />
)

const ChartHoriz_dummy = ({ onPress, img, keterangan, jumlah }) => (
    <TouchableHighlight underlayColor={'#043353'} onPress={onPress}>
        <View style={styles.chartHorizVw}>
            <Image
                style={{ width: '100%' }}
                source={img}
            />
            <Text style={styles.chartHorizTxt}>{keterangan}</Text>
            {/* <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{jumlah}</Text> */}
        </View>
    </TouchableHighlight>
)

const ChartTool_dummy = ({ dipinjam, dikembalikan }) => (
    <View style={styles.chartRenRealVw}>
        <Image
            style={styles.chartRenRealImg}
            source={IMAGES.CHART_HISTOGRAM}
        />
        <View style={styles.chartRenRealRow}>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'left' }]}>{'Jumlah Tools\nDipinjam'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{dipinjam} </Text>
            </View>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'right' }]}>{'Jumlah Tools\nDikembalikan'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'right' }]}>{dikembalikan} </Text>
            </View>
        </View>
    </View>
)

const ChartProyek = ({ total }) => (
    <ProgressChart
        hideLegend={true}
        data={{
            labels: ["Andi",],
            data: [0.5]
        }}
        width={0.7 * wScreen} // from react-native
        height={.4 * wScreen}
        // height={220}
        chartConfig={{
            // backgroundGradientFrom: '#0B2E6A',
            // backgroundGradientTo: 'purple',
            // color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0.0,
            color: (opacity = 1) => `rgba(236, 50, 71, ${opacity})`,
            strokeWidth: 1, // optional, default 3
        }}
    // backgroundColor="green"
    // paddingLeft="15"
    // absolute
    // style={{
    //     marginVertical: 8,
    //     borderRadius: 16
    // }}
    />
)

const CardProyek = ({ total }) => (
    <View style={styles.proyCard}>
        <Text style={styles.proyTxt}>
            Total Proyek
        </Text>
        <View style={styles.proyRow}>
            <Text style={styles.proyTotTxt}>{total}</Text>
            <Image
                style={[styles.iconImg, styles.proyIconSize]}
                source={IMAGES.KUNCI_PAS}
            // source={require("../../assets/icons/kunci_pas.png")}
            />
        </View>
    </View>
)
const dataToolProy_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'selesai' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'selesai' },
];

const dataToolDikirim_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
]



function Component({ navigation }) {
    const [dataToolProy, setDataToolProy] = useState([])
    const [dataToolDikirim, setDataToolDikirim] = useState([])
    const [isLoggedIn, setIsLoggedIn] = useState(false)
    const [userType, setUserType] = useState(null)

    useEffect(() => {
        setDataToolProy(dataToolProy_dummy)
        setDataToolDikirim(dataToolDikirim_dummy)

        getLogin()
        return () => {
            // cleanup
        }
    }, [dataToolProy, dataToolDikirim])

    getLogin = async () => {
        try {
            AsyncStorage.getItem('authInfo').then((val) => {
                // if (!val) {
                //     navigation.navigate('Login')
                // }
                console.log('home,getitem', val)
            })
            // const userTypex = await AsyncStorage.getItem('userType').then((val) => setUserType(val))
            // console.log('isLoggedIn', isLoggedInx)
            // console.log('userType', userTypex)
        } catch (e) {
            console.log('fail getlogin info')
            // error reading value
        }
    }

    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >

                            {/* <ChartTool_dummy dipinjam={100} dikembalikan={60} /> */}
                            <PieChartTool dipinjam={100} dikembalikan={60} />
                            <CardToolProy data={dataToolProy} tahun={2020} />
                            <CardToolDikirim data={dataToolDikirim} />

                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
