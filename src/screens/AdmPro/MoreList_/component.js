import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button } from 'react-native-paper';
import { TouchableOpacity } from 'react-native-gesture-handler';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

function Component({ navigation }) {
    return (
        <SafeAreaView
            style={styles.container}
        >
            <ScrollView style={styles.scrollView}>
                <Image style={styles.bgImage} source={IMAGES.PETIR} />
                <Image style={[styles.bgImage, styles.overlayImage]} source={IMAGES.PEMBANGKIT} />

                <View style={styles.content}>
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignSelf: 'flex-start' }}>
                        <Text style={{ fontWeight: 'bold', padding: 5, fontSize: .04 * wScreen, color: 'white' }}>Forms</Text>
                    </View>

                    <View style={styles.rowMenu}>
                        <TouchableOpacity onPress={() => navigation.navigate('PeminjamanToolForm')}>
                            <View style={[styles.cardMenu, styles.clrOrange]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>{'Pinjam\nTool\nProyek'}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('PengembalianToolPusatForm')}>
                            <View style={[styles.cardMenu, styles.clrOrange]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>{'Kembali\nTool\nPusat'}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('PengembalianToolProyekForm')}>
                            <View style={[styles.cardMenu, styles.clrOrange]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>{'Kembali\nTool\nUser'}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    {/* 
                    <View style={{ flex: 1, justifyContent: 'flex-start', alignSelf: 'flex-start' }}>
                        <Text style={{ fontWeight: 'bold', padding: 5, fontSize: .04 * wScreen, color: 'white' }}>Histories</Text>
                    </View>

                    <View style={styles.rowMenu}>
                        <TouchableOpacity>
                            <View style={[styles.cardMenu, styles.clrGrey]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>Report</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={[styles.cardMenu, styles.clrGrey]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>Report</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={[styles.cardMenu, styles.clrGrey]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>Report</Text>
                            </View>
                        </TouchableOpacity>
                    </View> */}

                    <View style={{ flex: 1, justifyContent: 'flex-start', alignSelf: 'flex-start' }}>
                        <Text style={{ fontWeight: 'bold', padding: 5, fontSize: .04 * wScreen, color: 'white' }}>Reports</Text>
                    </View>
                    <View style={styles.rowMenu}>
                        <TouchableOpacity onPress={() => navigation.navigate('ReportGapTool')}>
                            <View style={[styles.cardMenu, styles.clrGreen]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>Gap Tool</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={[styles.cardMenu, styles.clrGrey]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>Report</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={[styles.cardMenu, styles.clrGrey]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>Report</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, justifyContent: 'flex-start', alignSelf: 'flex-start' }}>
                        <Text style={{ fontWeight: 'bold', padding: 5, fontSize: .04 * wScreen, color: 'white' }}>Others</Text>
                    </View>
                    <View style={styles.rowMenu}>
                        <TouchableOpacity onPress={() => navigation.navigate('StokGudang')}>
                            <View style={[styles.cardMenu, styles.clrPink]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>{'Stok\nGudang'}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={[styles.cardMenu, styles.clrGrey]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>Report</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <View style={[styles.cardMenu, styles.clrGrey]}>
                                <Image style={styles.iconMenu} source={IMAGES.KUNCI_PAS} />
                                <Text style={styles.txtMenu}>Report</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

            </ScrollView>
        </SafeAreaView >
    )
}

export default Component
