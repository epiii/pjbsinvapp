import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { FlatList, TextInput, ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FAB } from 'react-native-paper';
import { FlatGrid, SectionGrid } from 'react-native-super-grid'
import IconFA from 'react-native-vector-icons/FontAwesome'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const ListTool = ({ items }) => {
    const navigation = useNavigation();
    return (
        <FlatGrid
            itemDimension={.3 * wScreen}
            data={items}
            style={styles.gridView}
            spacing={10}
            renderItem={({ item }) => (
                <RenderItem item={item} />
            )}
        />
    );
}

const RenderItem = ({ item }) => {
    const navigation = useNavigation();

    return (
        <TouchableOpacity
            underlayColor={'aqua'}
            onPress={() => navigation.navigate(item.link)}
        >
            <View style={[styles.itemContainer, {
                // backgroundColor: 'red'
                borderRadius: .2 * wScreen,
                // padding: 90,
            }]}>
                {/* <IconFA name={item.icon} color={'white'} size={.09 * wScreen} /> */}
                <View style={styles.thumbOverlay}></View>
                <Image style={styles.thumbImg} source={item.img} />

                {item.isAvailable ? (<Text style={styles.thumbAvailTxt}>{item.isAvailable ? 'Tersedia' : ''}</Text>) : null}
                {item.isCalibrated ? (<Text style={styles.thumbCalibTxt}>{item.isCalibrated ? 'Kalibrasi' : ''}</Text>) : null}
                <Text style={styles.thumbNoTxt}>{item.inventoryNo}</Text>
                <Text style={styles.thumbNameTxt}>{item.name}</Text>
                <Text style={styles.thumbCatTxt}>{item.category}</Text>
            </View>
        </TouchableOpacity>
    )
}

const dataSwitchTab = [
    { id: 1, menu: 'Specific Tools', link: 'Tool AAA', },
    { id: 2, menu: 'Specific Tools', link: 'Tool AAA', },
    { id: 3, menu: 'General Tools', link: 'Tool AAA', },
    { id: 4, menu: 'Consumable Tools', link: 'Tool AAA', },
]

const items = [
    { id: 1, link: 'DetailKalibrasi', inventoryNo: '11122', name: 'Tool AAA', category: 'Utilisasi', isCalibrated: true, isAvailable: true, img: IMAGES.TOOL_1 },
    { id: 1, link: 'DetailKalibrasi', inventoryNo: '11122', name: 'Tool BBB', category: 'Utilisasi', isCalibrated: false, isAvailable: false, img: IMAGES.TOOL_2 },
    { id: 1, link: 'DetailKalibrasi', inventoryNo: '11122', name: 'Tool CCC', category: 'Utilisasi', isCalibrated: true, isAvailable: true, img: IMAGES.TOOL_3 },
    { id: 1, link: 'DetailKalibrasi', inventoryNo: '11122', name: 'Tool DDD', category: 'Utilisasi', isCalibrated: true, isAvailable: false, img: IMAGES.TOOL_4 },
    { id: 1, link: 'DetailKalibrasi', inventoryNo: '11122', name: 'Tool EEE', category: 'Utilisasi', isCalibrated: true, isAvailable: true, img: IMAGES.TOOL_1 },
]
const MyTextInput = ({ isMultiLine, value, name, textAlign, placeholder, label }) => (
    <TextInput
        style={[styles.txtInput, {
            textAlign: textAlign ? textAlign : 'left',
            textAlignVertical: isMultiLine ? 'top' : 'center',
        }]}
        numberOfLines={isMultiLine ? 3 : 1}
        multiline={isMultiLine ? true : false}

        name={name}
        value={value}
        placeholder={placeholder}
        placeholderTextColor="grey"
    />
)
const SwitchTab = ({ data }) => (
    <View style={styles.switchTab}>
        {/* {data.map((item, index) => (
            <Text>Spec Tools</Text>
        ))} */}
        <FlatList
            data={data}
            horizontal={true}
            renderItem={renderItem}
            keyExtractor={(item) => item.id}
            extraData={1}
        />
    </View>
)
const Item = ({ item, onPress, style }) => (
    <TouchableOpacity
        onPress={onPress}
        style={[styles.itemVw, style]}
    >
        <Text style={styles.itemTxt}>{item.menu}</Text>
    </TouchableOpacity>
);

const renderItem = ({ item }) => {
    const borderColor = item.id === 1 ? "white" : "transparent";
    // const backgroundColor = item.id === 1 ? "#6e3b6e" : "#f9c2ff";
    // const backgroundColor = item.id === selectedId ? "#6e3b6e" : "#f9c2ff";

    return (
        <Item
            item={item}
            onPress={() => { }}
            // onPress={() => alert(`selected ${item.id}`)}
            // onPress={() => setSelectedId(item.id)}
            style={{
                borderBottomColor: borderColor,
                borderBottomWidth: 2,
                marginRight: .05 * wScreen,
                paddingBottom: .035 * wScreen,
                // backgroundColor: backgroundColor,
            }}
        />
    );
}
function Component({ navigation }) {
    // const [items, setItems] = React.useState([
    //     { name: 'ASBESTOS', code: '#7f8c8d', iconscreen:'PeminjamanToolForm',: 'plus' },

    // ]);
    useEffect(() => {
        return () => {
            // cleanup
        }
    }, [])

    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                {/* <SafeAreaView style={styles.safeAreaView}> */}
                {/* <ScrollView contentContainerStyle={{ flexGrow: 1 }} > */}
                <View style={{
                    flex: 1,
                    paddingHorizontal: .05 * wScreen
                }} >
                    <View style={{
                        flexDirection: 'row',
                        paddingHorizontal: .06 * wScreen,
                        backgroundColor: 'white',
                        // height: 50,
                        borderRadius: .03 * wScreen,
                        alignItems: 'center',
                        marginHorizontal: .025 * wScreen,
                    }}>
                        <IconFA name={'search'} size={.05 * wScreen} color={'#2A2A2A'} />
                        <MyTextInput name={'noOrder'} placeholder={'Cari Tools Disini ...'} />
                    </View>

                    <SwitchTab data={dataSwitchTab} />
                    <ListTool items={items} />
                </View>
                {/* </ScrollView> */}
                {/* </SafeAreaView> */}

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
