import { Dimensions, StyleSheet } from 'react-native';
const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const styles = StyleSheet.create({
    itemContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: .03 * wScreen,
        overflow: "hidden",
    },
    modalForm: {
        backgroundColor: '#0B2E6A',
        marginHorizontal: .08 * wScreen,
        marginVertical: .2 * wScreen,
        padding: .05 * wScreen,
        borderRadius: .035 * wScreen,
        flex: 1,
    },
    thumbOverlay: {
        backgroundColor: 'black',
        zIndex: 2,
        width: '100%',
        // padding: 100,
        position: 'absolute',
        top: 0,
        opacity: .4,
        bottom: 0,
    }, modalButton: {
        // alignSelf: 'fle',
        // backgroundColor: '#5CC652',
        color: 'white',
        borderRadius: .06 * wScreen,
        paddingVertical: .01 * wScreen,
        paddingHorizontal: .04 * wScreen,
    },
    thumbCalibTxt: {
        marginHorizontal: .03 * wScreen,
        marginTop: .015 * wScreen,

        zIndex: 3,
        backgroundColor: '#F2994A',
        borderRadius: .015 * wScreen,
        paddingVertical: .01 * wScreen,
        paddingHorizontal: .015 * wScreen,
        color: 'white',
        fontSize: .033 * wScreen,
        position: 'absolute',
        top: .03 * wScreen,
        left: .02 * wScreen,
    }, thumbNoTxt: {
        marginHorizontal: .03 * wScreen,

        zIndex: 3,
        color: 'white',
        fontSize: .033 * wScreen,
        position: 'absolute',
        bottom: .21 * wScreen,
        left: .02 * wScreen,
    },
    thumbNameTxt: {
        marginHorizontal: .03 * wScreen,

        zIndex: 3,
        color: 'white',
        fontSize: .044 * wScreen,
        fontWeight: 'bold',
        position: 'absolute',
        bottom: .14 * wScreen,
        left: .02 * wScreen,
    },
    thumbCatTxt: {
        marginHorizontal: .02 * wScreen,

        zIndex: 1,
        fontSize: .035 * wScreen,
        color: 'yellow',
        fontWeight: 'bold',
        position: 'absolute',
        bottom: .04 * wScreen,
        left: .02 * wScreen,
    },
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0B2E6A'
    },
    bgImage: {
        width: '100%',
        height: '100%',
        // flex: 1,
        // resizeMode: 'cover', // or 'stretch'
    },
    chartProySize: {
        height: .45 * wScreen,
        width: .45 * wScreen,
    },
    chartRenRealImg: {
        width: '100%'
    },
    chartRenRealRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    chartRenRealItem: {

    },
    chartRenRealTxt: {
        color: 'white',
        marginTop: .04 * wScreen,
        marginBottom: .001 * wScreen,
        fontSize: .05 * wScreen,
        fontWeight: 'bold'
    },
    chartHorizTxt: {
        color: 'white',
        marginTop: -.02 * wScreen,
        marginBottom: .001 * wScreen,
        fontSize: .05 * wScreen,
        fontWeight: 'bold'
    },
    chartRenRealTxtNum: {
        color: 'white',
        marginVertical: .0001 * wScreen,
        fontSize: .10 * wScreen,
        fontWeight: 'bold'
    },
    chartRenRealVw: {
        paddingTop: .08 * wScreen,
        paddingBottom: .04 * wScreen,
        // height: .08 * wScreen,
        // width: .08 * wScreen,
    },
    chartHorizVw: {
        paddingVertical: .003 * wScreen,
    },
    fab: {
        position: 'absolute',
        margin: 16,
        backgroundColor: '#043353',
        right: 0,
        left: 0,
        bottom: -0.06 * wScreen,
        zIndex: 99
    },
    renProyTopCard: {
        justifyContent: 'center',
        backgroundColor: '#C4C4C4',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flexDirection: 'column'
    },
    proyCard: {
        padding: .022 * wScreen,
        backgroundColor: 'white',
        // height: .40 * wScreen,
        width: .40 * wScreen,
        borderRadius: 20,
        flexDirection: 'column'
    },
    proyTxt: {
        color: '#EC3247',
        fontSize: .057 * wScreen,
        fontWeight: 'bold',
        justifyContent: 'flex-start',
        textAlign: 'left',
    },
    proyTotTxt: {
        textAlign: 'right',
        padding: 10,
        fontSize: .08 * wScreen,
        color: 'black',
        fontWeight: 'bold'
    },
    vwRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        marginVertical: .08 * wScreen,
    },
    vwItem: {
        // height: '50%',
        // width: '50%',
    },
    proyRow: {
        flexDirection: 'row',
        justifyContent: 'flex-end'
    },
    txtHeadListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        textAlign: 'justify',
        fontSize: .05 * wScreen,
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold'
    },
    btnListRencanaPro: {
        alignItems: 'center',
        alignSelf: 'center',
        backgroundColor: '#EC3247',
        color: 'white',
        borderRadius: .04 * wScreen,
        paddingVertical: .01 * wScreen,
        paddingHorizontal: .04 * wScreen,
        justifyContent: 'center',
    },
    safeAreaView: {
        // alignItems: 'center',
        // flex: 1,
        justifyContent: 'space-around',
        padding: .05 * wScreen
    },
    txtListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        textAlign: 'center',
        color: 'black',
        // fontWeight: 'bold',
        // margin:-5,
        // height:10
    },
    vwListRencanaPro: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    renProyContentCard: {
        backgroundColor: 'white',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: .05 * wScreen,
        paddingVertical: .02 * wScreen,
    },
    iconImg: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center'
    },
    overlayImage: {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        justifyContent: "center",
        alignItems: "center",
    },
    content: {
        position: "absolute",
        // top: 0,
        // right: 0,
        // bottom: 0,
        // left: 0,
        justifyContent: "center",
        alignItems: "center",
    },
    bigBlue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30,
    },
    dataTable: {
        marginTop: 5,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    filter: {
        flexDirection: 'row',
        paddingVertical: .01 * wScreen,
        paddingHorizontal: .05 * wScreen,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
        backgroundColor: 'white',
        borderRadius: .04 * wScreen,
    },
    mainText: {
        color: 'white'
    },
    section: {
        marginVertical: .05 * wScreen,
        fontSize: .04 * wScreen,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },
    sectionSub: {
        color: 'white',
        textAlign: 'center'
    }
});
export default styles