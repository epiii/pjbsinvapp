import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width

const styles = StyleSheet.create({
    fab: {
        position: 'absolute',
        margin: 16,
        right: 0,
        bottom: 0,
        color: 'blue'
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        width: .15 * wScreen,
        height: .15 * wScreen,
        borderRadius: .1 * wScreen,
        position: 'absolute',
        top: -.042 * wScreen,
        // width: .2 * wScreen,
        // height: .2 * wScreen,
        // top: -60,
        // top: -35,
        // shadowColor: '#7F58FF',
        // shadowRadius: 5,
        // shadowOffset: { height: 10 },
        // shadowOpacity: 0.3,
        // borderWidth: 3,
        zIndex: 99

    }
});
export default styles
