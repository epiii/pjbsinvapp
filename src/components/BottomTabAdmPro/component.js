import React from 'react'
import {
    HomeAdmProStackScreen,
    HistoryStackScreen,
    // HistoryAdmProStackScreen,
    ToolStackScreen,
    MoreStackScreen,
    AccountStackScreen,
    AddStackScreen,
    NotificationStackScreen,
    AuthStackScreen
} from '../../configs/Routes'
// import PeminjamanToolFormScreen from '../../screens/AdmPro/PeminjamanToolForm'

// import MaterialCommunityIcons  from 'react-native-vector-icons/MaterialCommunityIcons '
import IconF from 'react-native-vector-icons/Feather'
import IconI from 'react-native-vector-icons/Ionicons'
import IconAD from 'react-native-vector-icons/AntDesign'
import IconFA from 'react-native-vector-icons/FontAwesome5'
import AddButton from '../AddButton'
import ReportGapToolScreen from '../../screens/AdmPro/ReportGapTool'

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import styles from './styles'
const Tab = createMaterialBottomTabNavigator()

const Component = () => (
    <Tab.Navigator
        initialRouteName="Dashboard"
        activeColor="#043353"
        inactiveColor="grey"
        barStyle={{ backgroundColor: 'white' }}
    >
        <Tab.Screen
            name="Dashboard"
            component={HomeAdmProStackScreen}
            options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color }) => (
                    <IconF name="home" color={color} size={26} />
                ),
            }}
        />
        <Tab.Screen
            name="Notifications"
            component={NotificationStackScreen}
            options={{
                tabBarLabel: 'Notification',
                tabBarIcon: ({ color }) => (
                    <IconF name="bell" color={color} size={26} />
                ),
            }}
        />
        <Tab.Screen
            name="Add"
            component={AddStackScreen}
            // component={PeminjamanToolFormScreen}
            options={{
                tabBarLabel: 'Add',
                tabBarIcon: ({ color }) => (
                    <AddButton icon={'plus'} bgColor={'#043353'}  fgColor={'white'} />
                ),
                // tabBarButton: () => (<AddButton bgColor={'#043353'} fgColor={'white'} />)
                // <View style={{
                //     position: 'relative',
                //     bottom: 35,
                //     alignItems: 'center',
                //     justifyContent: 'space-around',
                //     height: 85
                // }}>
                //     <IconF name="bell" color={'red'} size={26} />
                //     {/* <Icon
                //         name="barcode-scan"
                //         type="material-community"
                //         reverse
                //         color={'yellow'}
                //         reverseColor='black'
                //         containerStyle={{ padding: 0, margin: 0, elevation: 5 }}
                //         onPress={() => console.log('Hi')}
                //         size={30} /> */}
                //     <Text>Scan</Text>
                // </View>
            }}
        />
        <Tab.Screen
            name="History"
            component={HistoryStackScreen}
            options={{
                tabBarLabel: 'History',
                tabBarIcon: ({ color }) => (
                    <IconFA name="history" color={color} size={21} />
                ),
            }}
        />
        {/* <Tab.Screen
            name="Tool"
            component={ToolStackScreen}
            options={{
                tabBarLabel: 'Tools',
                tabBarIcon: ({ color }) => (
                    <IconFA name="boxes" color={color} size={21} />
                ),
            }}
        /> */}
        <Tab.Screen
            name="More"
            component={MoreStackScreen}
            options={{
                tabBarLabel: 'More',
                tabBarIcon: ({ color }) => (
                    <IconAD name="appstore-o" color={color} size={26} />
                ),
            }}
        />
        {/* <Tab.Screen
            name="Account"
            component={AccountStackScreen}
            options={{
                tabBarLabel: 'Account',
                tabBarIcon: ({ color }) => (
                    <IconAD name="user" color={color} size={26} />
                ),
                tabBarVisible:false
            }}
        /> */}
    </Tab.Navigator>
)

export default Component