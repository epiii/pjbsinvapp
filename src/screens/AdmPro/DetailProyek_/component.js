import React, { useEffect } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button } from 'react-native-paper';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

function Component({ navigation, route }) {
    useEffect(() => {
        if (route.params?.namaProyek) {
            //todo 
        }
    }, [route.params?.namaProyek])

    return (
        <SafeAreaView
            style={styles.container}
        >
            <ScrollView
                contentContainerStyle={{ flexGrow: 1 }}
            >
                <View style={{
                    height: hScreen
                }}>
                    <Image style={styles.bgImage} source={IMAGES.PETIR} />
                    <Image style={[styles.bgImage, styles.overlayImage]} source={IMAGES.PEMBANGKIT} />

                    <View style={styles.content}>
                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: .04 * wScreen, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ flexWrap: 'wrap', color: 'white', textAlign: 'justify' }}>{route.params?.deskProyek}</Text>
                            </View>
                        </View>

                        <View style={{ marginTop: 10, flex: 1, flexDirection: 'row', paddingTop: .04 * wScreen, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1, }}>
                                <Text style={styles.txtLabel}>Lokasi Proyek</Text>
                            </View>
                            <View style={{ flex: 1, marginRight: 20, }}>
                                <Text style={{ color: 'white', textAlign: 'right' }}>UBJOM Gresik</Text>
                            </View>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: .04 * wScreen, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1, }}>
                                <Text style={styles.txtLabel}>Status</Text>
                            </View>
                            <View style={styles.vwStatus}>
                                <Text style={styles.txtStatus}>Yang Akan Datang</Text>
                            </View>
                        </View>

                        <View style={styles.cardContent}>
                            <View style={styles.listRow}>
                                <Text style={styles.listLabel}>Start Proj</Text>
                                <Text style={styles.listField}> DD/MM/YYY</Text>
                            </View>
                            <View style={styles.listRow}>
                                <Text style={styles.listLabel}>End Proj</Text>
                                <Text style={styles.listField}> DD/MM/YYY</Text>
                            </View>
                        </View>

                        <View style={styles.cardContent}>
                            <View style={styles.listRow}>
                                <Text style={styles.listLabel}>Customer</Text>
                                <Text style={styles.listField}>John Doe</Text>
                            </View>
                            <View style={styles.listRow}>
                                <Text style={styles.listLabel}>Warehouse</Text>
                                <Text style={styles.listField}>Blok A2</Text>
                            </View>
                            <View style={styles.listRow}>
                                <Text style={styles.listLabel}>Tipe Proyek</Text>
                                <Text style={styles.listField}>Pasang Kabel</Text>
                            </View>
                            <View style={styles.listRow}>
                                <Text style={styles.listLabel}>Manajer Proyek</Text>
                                <Text style={styles.listField}>Mr. ABCD</Text>
                            </View>
                            <View style={styles.listRow}>
                                <Text style={styles.listLabel}>Rendal Proyek</Text>
                                <Text style={styles.listField}>AXBJSDF</Text>
                            </View>
                        </View>

                        <Button color='white' onPress={() => navigation.navigate('PersiapanToolForm', { namaProyek: route.params?.namaProyek })} style={styles.btnListRencanaPro}>Siapkan Tools</Button>
                    </View>
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}

export default Component
