import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInput, ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { Button, FAB, DataTable } from 'react-native-paper';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const CardToolProy = ({ tahun, data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>
            <View style={styles.toolProyTopCard}>
                <Text style={styles.txtHeadListRencanaPro}>
                    Status Tools Proyek {tahun}
                </Text>
            </View>

            <View style={styles.toolProyContentCard}>
                {data.map((item, key) => (
                    <TouchableOpacity
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                            status: item.status,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {key + 1}. {item.nama}
                            </Text>
                            <Label fgColor={'white'} bgColor={'#EC3247'} text={item.status.toUpperCase()} />
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

const CardToolDikirim = ({ data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>
            <View style={styles.toolProyTopCard}>
                <Text style={styles.txtHeadListRencanaPro}>
                    Tool Tool Dikirim
                </Text>
            </View>

            <View style={styles.toolProyContentCard}>
                {data.map((item, key) => (
                    <TouchableOpacity
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                            status: item.status,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {key + 1}. {item.nama}
                            </Text>
                            <Label fgColor={'white'} bgColor={'#EC3247'} text={item.status.toUpperCase()} />
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

const ChartProyek_dummy = ({ total }) => (
    <Image
        style={[styles.iconImg, styles.chartProySize]}
        source={IMAGES.CHART_CIRCLE}
    // source={require("../../assets/images/chart_circle.png")}
    />
)

const ChartHoriz_dummy = ({ onPress, img, keterangan, jumlah }) => (
    <TouchableHighlight underlayColor={'#043353'} onPress={onPress}>
        <View style={styles.chartHorizVw}>
            <Image
                style={{ width: '100%' }}
                source={img}
            />
            <Text style={styles.chartHorizTxt}>{keterangan}</Text>
            {/* <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{jumlah}</Text> */}
        </View>
    </TouchableHighlight>
)

const ChartTool_dummy = ({ dipinjam, dikembalikan }) => (
    <View style={styles.chartRenRealVw}>
        <Image
            style={styles.chartRenRealImg}
            source={IMAGES.CHART_HISTOGRAM}
        />
        <View style={styles.chartRenRealRow}>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'left' }]}>{'Jumlah Tools\nDipinjam'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{dipinjam} </Text>
            </View>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'right' }]}>{'Jumlah Tools\nDikembalikan'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'right' }]}>{dikembalikan} </Text>
            </View>
        </View>
    </View>
)

const ChartProyek = ({ total }) => (
    <ProgressChart
        hideLegend={true}
        data={{
            labels: ["Andi",],
            data: [0.5]
        }}
        width={0.7 * wScreen} // from react-native
        height={.4 * wScreen}
        // height={220}
        chartConfig={{
            // backgroundGradientFrom: '#0B2E6A',
            // backgroundGradientTo: 'purple',
            // color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0.0,
            color: (opacity = 1) => `rgba(236, 50, 71, ${opacity})`,
            strokeWidth: 1, // optional, default 3
        }}
    // backgroundColor="green"
    // paddingLeft="15"
    // absolute
    // style={{
    //     marginVertical: 8,
    //     borderRadius: 16
    // }}
    />
)

const CardProyek = ({ total }) => (
    <View style={styles.proyCard}>
        <Text style={styles.proyTxt}>
            Total Proyek
        </Text>
        <View style={styles.proyRow}>
            <Text style={styles.proyTotTxt}>{total}</Text>
            <Image
                style={[styles.iconImg, styles.proyIconSize]}
                source={IMAGES.KUNCI_PAS}
            // source={require("../../assets/icons/kunci_pas.png")}
            />
        </View>
    </View>
)
const dataToolProy_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
];

const dataToolDikirim_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
]

const MyLabel = ({ text, color }) => (
    <View style={[styles.labelVw, { backgroundColor: color }]}>
        <Text style={styles.labelTxt}>{text}</Text>
    </View>
)

const MyButton = () => (
    <View style={styles.button}>
        <Button
            color='white'
            style={[styles.btnListRencanaPro, { backgroundColor: '#FFAC30' }]}
            onPress={() => alert('under development ....')}
        >Cek Lampiran</Button>
    </View>
)

const MyField = ({ value, nama, placeholder, label }) => (
    <View style={styles.fieldArea} >
        <Text style={styles.fieldLabel}>{label}</Text>
        <TextInput
            style={styles.txtInput}
            value={value}
            // underlineColorAndroid="transparent"
            placeholder={placeholder}
            placeholderTextColor="grey"
        />
    </View>
)

const MyText = ({ value, label }) => (
    <View style={styles.fieldArea} >
        <Text style={styles.fieldLabel}>{label} :</Text>
        <Text style={styles.fieldValue}>{value}</Text>
    </View>
)
const MyTable = () => (
    <View style={styles.tableArea}>
        <Text style={styles.tableLabel}>Tools yang dipinjam</Text>
        <DataTable style={styles.dataTable}>
            <DataTable.Header>
                <DataTable.Title>No Inv</DataTable.Title>
                <DataTable.Title numeric>Tool</DataTable.Title>
                <DataTable.Title numeric>Qty</DataTable.Title>
                <DataTable.Title >Status</DataTable.Title>
            </DataTable.Header>

            <DataTable.Row>
                <DataTable.Cell>Mrs BBB</DataTable.Cell>
                <DataTable.Cell >Beko Y</DataTable.Cell>
                <DataTable.Cell numeric>100 </DataTable.Cell>
                <DataTable.Cell ><MyLabel color={'#5CC652'} text={'aman'} /></DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR ZZZ</DataTable.Cell>
                <DataTable.Cell >Traktor</DataTable.Cell>
                <DataTable.Cell numeric>5</DataTable.Cell>
                <DataTable.Cell ><MyLabel color={'#EC3247'} text={'rusak'} /></DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR AAA</DataTable.Cell>
                <DataTable.Cell >Forklift X</DataTable.Cell>
                <DataTable.Cell numeric>10</DataTable.Cell>
                <DataTable.Cell ><MyLabel color={'#CBCBCB'} text={'habis'} /></DataTable.Cell>
            </DataTable.Row>

            <DataTable.Pagination
                page={1}
                numberOfPages={3}
                onPageChange={page => {
                    console.log(page);
                }}
                label="1-3 of 6"
            />
        </DataTable>
    </View>
);

function Component({ navigation }) {
    const [dataToolProy, setDataToolProy] = useState([])
    const [dataToolDikirim, setDataToolDikirim] = useState([])

    useEffect(() => {
        setDataToolProy(dataToolProy_dummy)
        setDataToolDikirim(dataToolDikirim_dummy)
        return () => {
            // cleanup
        }
    }, [dataToolProy, dataToolDikirim])

    return (
        <ImageBackground
            // source={IMAGES.PETIR}
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >
                            <MyField
                                name={'user'}
                                placeholder={'User'}
                                label={'User'}
                            />
                            <MyField
                                value={'DD/MM/YYY - DD/MM/YYY'}
                                name={'tanggal'}
                                placeholder={'Tanggal'}
                                label={'Tanggal'}
                            />
                            <MyField
                                name={'tipePekerjaan'}
                                placeholder={'Tipe Pekerjaan'}
                                label={'Tipe Pekerjaan'}
                            />
                            <MyTable />
                            <MyText label={'Keterangan'} value={'ini keterangan bla bla bla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla blabla bla bla bla bla bla bla bla '} />
                            <MyButton />

                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground >
    )
}

export default Component
