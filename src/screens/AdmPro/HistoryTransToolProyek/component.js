import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInput, ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { Button, FAB, DataTable } from 'react-native-paper';
import MyFieldHoriz from '../../../components/MyFieldHoriz'
import MyText from '../../../components/MyText'
import MyLabel from '../../../components/MyLabel'
import MyTextInput from '../../../components/MyTextInput'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const dataToolProy_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
];

const dataToolDikirim_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
]

const MyField = ({ value, nama, placeholder, label }) => (
    <View style={styles.fieldArea} >
        <Text style={styles.fieldLabel}>{label}</Text>
        <TextInput
            style={styles.txtInput}
            value={value}
            // underlineColorAndroid="transparent"
            placeholder={placeholder}
            placeholderTextColor="grey"
        />
    </View>
)
const MyButton = () => {
    const navigation = useNavigation();

    return (
        <View style={styles.button}>
            <Button
                color='white'
                style={[styles.btnListRencanaPro, { backgroundColor: '#FFAC30' }]}
                onPress={() => navigation.navigate('HistoryUser')}
            >Cek History User</Button>
        </View>
    )
}
const MyTable = () => (
    <View style={styles.tableArea}>
        <Text style={styles.tableLabel}>Tools yang dipinjam</Text>
        <DataTable style={styles.dataTable}>
            <DataTable.Header>
                <DataTable.Title>User</DataTable.Title>
                <DataTable.Title numeric>Tool</DataTable.Title>
                <DataTable.Title numeric>Qty</DataTable.Title>
                <DataTable.Title >Status</DataTable.Title>
            </DataTable.Header>

            <DataTable.Row>
                <DataTable.Cell>Mrs BBB</DataTable.Cell>
                <DataTable.Cell >Beko Y</DataTable.Cell>
                <DataTable.Cell numeric>100 </DataTable.Cell>
                <DataTable.Cell ><MyLabel color={'#5CC652'} text={'stand by'} /></DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR ZZZ</DataTable.Cell>
                <DataTable.Cell >Traktor</DataTable.Cell>
                <DataTable.Cell numeric>5</DataTable.Cell>
                <DataTable.Cell ><MyLabel color={'#EC3247'} text={'digunakan'} /></DataTable.Cell>
            </DataTable.Row>

            <DataTable.Pagination
                page={1}
                numberOfPages={3}
                onPageChange={page => {
                    console.log(page);
                }}
                label="1-3 of 6"
            />
        </DataTable>
    </View>
);

function Component({ navigation }) {
    const [dataToolProy, setDataToolProy] = useState([])
    const [dataToolDikirim, setDataToolDikirim] = useState([])

    useEffect(() => {
        setDataToolProy(dataToolProy_dummy)
        setDataToolDikirim(dataToolDikirim_dummy)
        return () => {
            // cleanup
        }
    }, [dataToolProy, dataToolDikirim])

    return (
        <ImageBackground
            // source={IMAGES.PETIR}
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >

                            <MyFieldHoriz
                                leftComponent={
                                    <MyText isBold={true} fgColor={'white'} text={'Tanggal'} />
                                }
                                rightComponent={
                                    <MyTextInput name={'tanggal'} placeholder={'DD/MM/YYY - DD/MM/YYY'} />
                                }
                                leftFlex={1}
                                rightFlex={2}
                            />
                            <MyFieldHoriz
                                leftComponent={
                                    <MyText isBold={true} fgColor={'white'} text={'Tipe Pekerjaan'} />
                                }
                                rightComponent={
                                    <MyTextInput name={'tipePekerjaan'} placeholder={'Pilih Tipe Pekerjaan'} />
                                }
                            />
                            <MyFieldHoriz
                                leftComponent={
                                    <MyText isBold={true} fgColor={'white'} text={'Area Pekerjaan'} />
                                }
                                rightComponent={
                                    <MyTextInput name={'areaPekerjaan'} placeholder={'Pilih Area Pekerjaan'} />
                                }
                            />

                            <MyTable />
                            <View style={styles.button}>
                                <Button
                                    color='white'
                                    style={[styles.btnListRencanaPro, { backgroundColor: '#FFAC30' }]}
                                    onPress={() => alert('cek history ...')}
                                >Cek History Transaksi</Button>
                            </View>
                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground >
    )
}

export default Component
