import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity, TextInput, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button, FAB, List, DataTable } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const MyList = () => {
    const navigation = useNavigation();
    return (
        <>
            <List.Section >
                <List.Subheader style={styles.listSubheader}>Pengajuan Tool</List.Subheader>
                <TouchableOpacity
                    onPress={() => navigation.navigate('NotificationDetail', {
                        id: '123',
                        notifTitle: 'Approval Proj AAA',
                        notifDesc: 'desk approval proj AAA oleh Mr. XXX jkfljskldjkjfkl lkjlksjdifusd sodufo psodi spodifod laksl;d alsdkhjdahsdjh ajsdhakjshdkajshdjahskdj',
                    })}
                >
                    <List.Item
                        style={styles.list}
                        title="Approval Proj AAA"
                        description="desk approval proj AAA oleh Mr. XXX"
                        left={props => <List.Icon {...props} icon="folder" />}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('NotificationDetail', {
                        id: '123',
                        notifTitle: 'Pengajuan Tool XX-555 ditolak',
                        notifDesc: 'Diberitahukan pengajuan Tool xx-555 stok kosong jkfljskldjkjfkl lkjlksjdifusd sodufo psodi spodifod laksl;d alsdkhjdahsdjh ajsdhakjshdkajshdjahskdj',
                    })}
                >
                    <List.Item
                        style={styles.list}
                        title="Pengajuan Tool XX-555 ditolak"
                        description="Diberitahukan pengajuan Tool xx-555 stok kosong"
                        left={props => <List.Icon {...props} icon="folder" />}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    onPress={() => navigation.navigate('NotificationDetail', {
                        id: '123',
                        notifTitle: 'Pengajuan XYZ',
                        notifDesc: 'Diberitahukan pengajuan Tool xx-555 stok kosong jkfljskldjkjfkl lkjlksjdifusd sodufo psodi spodifod laksl;d alsdkhjdahsdjh ajsdhakjshdkajshdjahskdj',
                    })}
                >
                    <List.Item
                        style={styles.list}
                        title="Pengajuan XYZ "
                        description="notif pengajuan tool XYZ sedang diproses..."
                        left={props => <List.Icon {...props} icon="folder" />}
                    />
                </TouchableOpacity>
            </List.Section>

            <List.Section>
                <List.Subheader style={styles.listSubheader}>Reminder</List.Subheader>
                <List.Item
                    style={[styles.list, styles.hasRead]}
                    title="Reminder Pengembalian Tool CL-123"
                    description="Diberitahukan untuk segera mengembalikan Tool CL-123"
                    left={props => <List.Icon {...props} icon="folder" />}
                />
                <List.Item
                    style={[styles.list, styles.hasRead]}
                    title="Selamat Hari raya idul fitri 1499 H "
                    description="Kami atas nama cabang YYY  mengucapakan selamat hari ra"
                    left={props => <List.Icon {...props} icon="folder" />}
                />
            </List.Section>
        </>)
};


function Component({ navigation, route }) {
    React.useEffect(() => {
        if (route.params?.namaProyek) {
            // Post updated, do something with `route.params.post`
            // For example, send the post to the server
        }
    }, [route.params?.namaProyek]);


    return (
        <SafeAreaView
            style={styles.container}
        >
            <ScrollView
                contentContainerStyle={{ flexGrow: 1 }}
            >
                <View style={{
                    height: 1.3 * hScreen
                }}>
                    <Image style={styles.bgImage} source={IMAGES.PETIR} />
                    <Image style={[styles.bgImage, styles.overlayImage]} source={IMAGES.PEMBANGKIT} />

                    <View style={styles.content}>
                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: .001 * wScreen, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ flexWrap: 'wrap', color: 'white', textAlign: 'justify' }}>{route.params?.namaProyek}</Text>
                            </View>
                        </View>

                        <MyList />

                    </View>
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}

// MyList.navigationOptions = () => {(
//     title:'Home'
// )}

// MyList['navigationOptions'] = screenProps => ({
//     title: 'Home'
// })

export default Component
