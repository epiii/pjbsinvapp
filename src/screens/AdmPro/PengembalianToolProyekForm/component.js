import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { Modal, TextInput, ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { Button, FAB, DataTable } from 'react-native-paper';
import MyFieldHoriz from '../../../components/MyFieldHoriz'
import IconAD from 'react-native-vector-icons/AntDesign'
import MySelectHoriz from '../../../components/MySelectHoriz'
import MyTextInput from '../../../components/MyTextInput'


const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const CardToolProy = ({ tahun, data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>
            <View style={styles.toolProyTopCard}>
                <Text style={styles.txtHeadListRencanaPro}>
                    Status Tools Proyek {tahun}
                </Text>
            </View>

            <View style={styles.toolProyContentCard}>
                {data.map((item, key) => (
                    <TouchableOpacity
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                            status: item.status,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {key + 1}. {item.nama}
                            </Text>
                            <Label fgColor={'white'} bgColor={'#EC3247'} text={item.status.toUpperCase()} />
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

const ChartProyek_dummy = ({ total }) => (
    <Image
        style={[styles.iconImg, styles.chartProySize]}
        source={IMAGES.CHART_CIRCLE}
    // source={require("../../assets/images/chart_circle.png")}
    />
)

const ChartHoriz_dummy = ({ onPress, img, keterangan, jumlah }) => (
    <TouchableHighlight underlayColor={'#043353'} onPress={onPress}>
        <View style={styles.chartHorizVw}>
            <Image
                style={{ width: '100%' }}
                source={img}
            />
            <Text style={styles.chartHorizTxt}>{keterangan}</Text>
            {/* <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{jumlah}</Text> */}
        </View>
    </TouchableHighlight>
)

const ChartTool_dummy = ({ dipinjam, dikembalikan }) => (
    <View style={styles.chartRenRealVw}>
        <Image
            style={styles.chartRenRealImg}
            source={IMAGES.CHART_HISTOGRAM}
        />
        <View style={styles.chartRenRealRow}>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'left' }]}>{'Jumlah Tools\nDipinjam'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{dipinjam} </Text>
            </View>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'right' }]}>{'Jumlah Tools\nDikembalikan'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'right' }]}>{dikembalikan} </Text>
            </View>
        </View>
    </View>
)

const ChartProyek = ({ total }) => (
    <ProgressChart
        hideLegend={true}
        data={{
            labels: ["Andi",],
            data: [0.5]
        }}
        width={0.7 * wScreen} // from react-native
        height={.4 * wScreen}
        // height={220}
        chartConfig={{
            // backgroundGradientFrom: '#0B2E6A',
            // backgroundGradientTo: 'purple',
            // color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0.0,
            color: (opacity = 1) => `rgba(236, 50, 71, ${opacity})`,
            strokeWidth: 1, // optional, default 3
        }}
    // backgroundColor="green"
    // paddingLeft="15"
    // absolute
    // style={{
    //     marginVertical: 8,
    //     borderRadius: 16
    // }}
    />
)

const CardProyek = ({ total }) => (
    <View style={styles.proyCard}>
        <Text style={styles.proyTxt}>
            Total Proyek
        </Text>
        <View style={styles.proyRow}>
            <Text style={styles.proyTotTxt}>{total}</Text>
            <Image
                style={[styles.iconImg, styles.proyIconSize]}
                source={IMAGES.KUNCI_PAS}
            // source={require("../../assets/icons/kunci_pas.png")}
            />
        </View>
    </View>
)

const dataToolDikirim2_dummy = [
    { id: 1, nama: 'Start Proyek', deskripsi: 'DD/MM/YYY', status: 'baru' },
    { id: 2, nama: 'Selesai Proyek', deskripsi: 'DD/MM/YYY', status: 'baru' },
]
const dataToolDikirim_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
]

const MyLabel = ({ text, color }) => (
    <View style={[styles.labelVw, { backgroundColor: color }]}>
        <Text style={styles.labelTxt}>{text}</Text>
    </View>
)

const MyButton2 = ({ text, color, onPress, element }) => (
    <TouchableOpacity onPress={onPress}>
        <View style={[styles.labelVw, { backgroundColor: color }]}>
            {element}
        </View>
    </TouchableOpacity>
)

const MyText = ({ text, fgColor, bgColor, textAlign, isBold }) => (
    <View>
        <Text style={{
            fontWeight: isBold ? 'bold' : 'normal',
            fontSize: isBold ? (.042 * wScreen) : (.037 * wScreen),
            color: fgColor ? fgColor : 'white',
            textAlign: textAlign ? textAlign : 'justify',
            backgroundColor: bgColor ? bgColor : 'transparent'
        }}>{text}</Text>
    </View>
)

const MyButton = ({ text, fgColor, bgColor, onPress }) => {
    const navigation = useNavigation();

    return (
        <Button
            compact={true}
            color={fgColor}
            style={[styles.myButton, { backgroundColor: bgColor }]}
            onPress={onPress}
        >{text}</Button>
    )
}

// const MyButton = () => {
//     const navigation = useNavigation();

//     return (
//         <View style={styles.button}>
//             <Button
//                 color='white'
//                 style={[styles.btnListRencanaPro, { backgroundColor: '#FFAC30' }]}
//                 onPress={() => navigation.navigate('HistoryUser')}
//             >Cek History User</Button>
//         </View>
//     )
// }

// const MyTextInput = ({ value, name, textAlign, placeholder, label }) => (
//     <TextInput
//         style={[styles.txtInput, {
//             textAlign: textAlign ? textAlign : 'left'
//         }]}
//         name={name}
//         value={value}
//         placeholder={placeholder}
//         placeholderTextColor="grey"
//     />
// )

const MyFieldVert = ({ topComponent, bottomComponent }) => (
    <View style={styles.fieldAreaVert} >
        <View style={styles.topComponent}>
            {topComponent ? topComponent : ''}
        </View>
        <View style={styles.bottomComponent}>
            {bottomComponent ? bottomComponent : ''}
        </View>
    </View>
)

const CardToolDikirim = ({ data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>

            <View style={styles.toolProyContentCard}>
                {data.map((item, index) => (
                    <TouchableOpacity
                        key={index}
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {item.nama}
                            </Text>
                            <Text style={[styles.txtListRencanaPro, { fontWeight: 'normal', color: 'black' }]}>
                                {item.deskripsi}
                            </Text>
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

const MyTableDipinjam_ = () => (
    <View style={styles.tableArea}>
        <Text style={styles.tableLabel}>Tools yang dipinjam</Text>
        <DataTable style={styles.dataTable}>
            <DataTable.Header>
                <DataTable.Title>No Inv</DataTable.Title>
                <DataTable.Title >Tool</DataTable.Title>
                <DataTable.Title numeric>Qty  </DataTable.Title>
                <DataTable.Title >Aksi</DataTable.Title>
            </DataTable.Header>

            <DataTable.Row>
                <DataTable.Cell>Mrs BBB</DataTable.Cell>
                <DataTable.Cell >Beko Y</DataTable.Cell>
                <DataTable.Cell numeric>100 </DataTable.Cell>
                <DataTable.Cell >
                    <MyButton2
                        onPress={() => alert('pop up')}
                        element={<IconAD name={'pluscircle'} size={26} color={'#0B2E6A'} />}
                        text={'plus'}
                    />
                </DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR ZZZ</DataTable.Cell>
                <DataTable.Cell >Traktor</DataTable.Cell>
                <DataTable.Cell numeric>5  </DataTable.Cell>
                <DataTable.Cell >
                    <MyButton2
                        onPress={() => alert('pop up')}
                        element={<IconAD name={'pluscircle'} size={26} color={'#0B2E6A'} />}
                        text={'plus'}
                    />
                </DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR ZZZ</DataTable.Cell>
                <DataTable.Cell >Traktor</DataTable.Cell>
                <DataTable.Cell numeric>5  </DataTable.Cell>
                <DataTable.Cell >
                    <MyButton2
                        onPress={() => alert('pop up')}
                        element={<IconAD name={'pluscircle'} size={26} color={'#0B2E6A'} />}
                        text={'plus'}
                    />
                </DataTable.Cell>
            </DataTable.Row>

            <DataTable.Pagination
                page={1}
                numberOfPages={3}
                onPageChange={page => {
                    console.log(page);
                }}
                label="1-3 of 6"
            />
        </DataTable>
    </View>
);

const MyTableDikembalikan_ = () => (
    <View style={styles.tableArea}>
        <Text style={styles.tableLabel}>Tools yang dikembalikan</Text>
        <DataTable style={styles.dataTable}>
            <DataTable.Header>
                <DataTable.Title>No Inv</DataTable.Title>
                <DataTable.Title >Tool</DataTable.Title>
                <DataTable.Title numeric>Qty  </DataTable.Title>
                <DataTable.Title >Kondisi</DataTable.Title>
            </DataTable.Header>

            <DataTable.Row>
                <DataTable.Cell>Mrs BBB</DataTable.Cell>
                <DataTable.Cell >Beko Y</DataTable.Cell>
                <DataTable.Cell numeric>100 </DataTable.Cell>
                <DataTable.Cell >
                    <MyButton2
                        onPress={() => alert('pop up')}
                        element={<IconAD name={'exclamationcircle'} size={26} color={'red'} />}
                        text={'plus'}
                    />
                </DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR ZZZ</DataTable.Cell>
                <DataTable.Cell >Traktor</DataTable.Cell>
                <DataTable.Cell numeric>5  </DataTable.Cell>
                <DataTable.Cell >
                    <MyButton2
                        onPress={() => alert('pop up')}
                        element={<IconAD name={'exclamationcircle'} size={26} color={'red'} />}
                        text={'plus'}
                    />
                </DataTable.Cell>
            </DataTable.Row>
            <DataTable.Row>
                <DataTable.Cell>MR ZZZ</DataTable.Cell>
                <DataTable.Cell >Traktor</DataTable.Cell>
                <DataTable.Cell numeric>5  </DataTable.Cell>
                <DataTable.Cell >
                    <MyButton2
                        onPress={() => alert('pop up')}
                        element={<IconAD name={'exclamationcircle'} size={26} color={'red'} />}
                        text={'plus'}
                    />
                </DataTable.Cell>
            </DataTable.Row>

            <DataTable.Pagination
                page={1}
                numberOfPages={3}
                onPageChange={page => {
                    console.log(page);
                }}
                label="1-3 of 6"
            />
        </DataTable>
    </View>
);

function Component({ navigation }) {
    const [dataToolDikirim, setDataToolDikirim] = useState([])
    const [dataToolDikirim2, setDataToolDikirim2] = useState([])
    const [modalVisible, setModalVisible] = useState(false);
    const [kondisiTool, setKondisiTool] = useState('');
    const [ketTool, setKetTool] = useState('');

    const [selectedRow, setSelectedRow] = useState({});
    const [selectedArr, setSelectedArr] = useState([]);
    const [availToolArr, setAvailToolArr] = useState([
        { inventoryNo: 'B001', name: 'Beko Y', qty: 100 },
        { inventoryNo: 'T002', name: 'Traktor Z', qty: 5 },
        { inventoryNo: 'FL03', name: 'Fork Lift X3', qty: 2 }
    ]);

    useEffect(() => {
        setDataToolDikirim(dataToolDikirim_dummy)
        setDataToolDikirim2(dataToolDikirim2_dummy)
        return () => {
            // cleanup
        }
    }, [dataToolDikirim2, dataToolDikirim])

    const handleAddTool = () => {
        setModalVisible(false)
        console.log('selRow', selectedRow)
        setSelectedArr(selectedArr => [...selectedArr, selectedRow])
    }

    const handleClickEdit = () => {
        setModalVisible(false)
        // setSelectedArr(selectedArr => [...selectedArr, selectedRow])
    }

    const openModalForm = (param) => {
        console.log('param', param)
        // setSelectedRow(param)
        setModalVisible(true)
    }

    const selectTool = (param) => {
        setSelectedArr(selectedArr => [...selectedArr, param])
    }

    const MyTableDipinjam = ({ selectToolFunc }) => (
        <View style={styles.tableArea}>
            <Text style={styles.tableLabel}>Tools yang dipinjam </Text>
            <DataTable style={styles.dataTable}>
                <DataTable.Header>
                    <DataTable.Title sortDirection='descending'>No Inv</DataTable.Title>
                    <DataTable.Title >Tool</DataTable.Title>
                    <DataTable.Title numeric>Qty  </DataTable.Title>
                    <DataTable.Title >Aksi</DataTable.Title>
                </DataTable.Header>

                {
                    availToolArr && availToolArr.map((data, i) => (
                        <DataTable.Row key={i} underlayColor={'red'} color={'red'}>
                            <DataTable.Cell>{data.inventoryNo}</DataTable.Cell>
                            <DataTable.Cell >{data.name}</DataTable.Cell>
                            <DataTable.Cell numeric>{data.qty} </DataTable.Cell>
                            <DataTable.Cell >
                                <MyButton2
                                    onPress={() => selectToolFunc({
                                        inventoryNo: data.inventoryNo,
                                        name: data.name,
                                        qty: data.qty,
                                    })}
                                    element={<IconAD name={'pluscircle'} size={26} color={'#0B2E6A'} />}
                                    text={'plus'}
                                />
                            </DataTable.Cell>
                        </DataTable.Row>
                    ))
                }
                <DataTable.Pagination
                    page={1}
                    numberOfPages={3}
                    onPageChange={page => {
                        console.log(page);
                    }}
                    label="1-3 of 6"
                />
            </DataTable>
        </View>
    );

    const MyTableDikembalikan = ({ selectedArr, selectToolFunc }) => (
        <View style={styles.tableArea}>
            <Text style={styles.tableLabel}>Tools yang dikembalikan </Text>
            <DataTable style={styles.dataTable}>
                <DataTable.Header>
                    <DataTable.Title sortDirection='descending'>No Inv</DataTable.Title>
                    <DataTable.Title >Tool</DataTable.Title>
                    <DataTable.Title numeric>Qty  </DataTable.Title>
                    <DataTable.Title >Aksi  </DataTable.Title>
                </DataTable.Header>
                {
                    selectedArr && selectedArr.map((data, i) => (
                        <DataTable.Row key={i}>
                            <DataTable.Cell>{data.inventoryNo}</DataTable.Cell>
                            <DataTable.Cell >{data.name}</DataTable.Cell>
                            <DataTable.Cell numeric>{data.qty + ' '} </DataTable.Cell>
                            <DataTable.Cell >
                                <MyButton2
                                    onPress={() => openModalForm(data.inventoryNo)}
                                    // onPress={() => handleDeleteRow(data.inventoryNo)}
                                    element={<IconAD name={'exclamationcircle'} size={26} color={'red'} />}
                                />
                            </DataTable.Cell>
                        </DataTable.Row>
                    ))
                }
                <DataTable.Pagination
                    page={1}
                    numberOfPages={3}
                    onPageChange={page => {
                        console.log(page);
                    }}
                    label="1-3 of 6"
                />
            </DataTable>
        </View>
    );

    return (
        <ImageBackground
            // source={IMAGES.PETIR}
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >

                            <MyFieldVert
                                topComponent={<MyText isBold={true} fgColor={'white'} text={'User'} />}
                                bottomComponent={
                                    <MyTextInput name={'noOrder'} placeholder={'Pilih User'} />
                                }
                            />
                            <MyFieldHoriz
                                leftComponent={<MyText isBold={true} fgColor={'white'} text={'Durasi'} />}
                                rightComponent={
                                    <MyTextInput name={'areaPekerjaan'} placeholder={'Pilih Durasi'} />
                                }
                            />
                            <MyFieldHoriz
                                leftComponent={<MyText isBold={true} fgColor={'white'} text={'Tipe Pekerjaan'} />}
                                rightComponent={
                                    <MyTextInput name={'tipePekerjaan'} placeholder={'Pilih Tipe Pekerjaan'} />
                                }
                            />

                            <MyTableDipinjam selectToolFunc={selectTool} />
                            <MyTableDikembalikan selectedArr={selectedArr} />

                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                <Button
                                    color={'white'}
                                    style={{
                                        flex: 1,
                                        backgroundColor: '#FFAC30',
                                        // alignSelf: 'center',
                                        borderRadius: .06 * wScreen,
                                        paddingVertical: .01 * wScreen,
                                        marginHorizontal: .02 * wScreen,
                                    }}
                                    onPress={() => alert('lampiran...')}
                                >Lampiran</Button>
                                <Button
                                    color={'white'}
                                    style={{
                                        flex: 1,
                                        marginHorizontal: .02 * wScreen,
                                        backgroundColor: '#5CC652',
                                        // alignSelf: 'center',
                                        borderRadius: .06 * wScreen,
                                        paddingVertical: .01 * wScreen,
                                    }}
                                    onPress={() => alert('Simpan...')}
                                >Simpan</Button>
                                <Modal
                                    animationType="slide"
                                    transparent={true}
                                    visible={modalVisible}
                                    onRequestClose={() => {
                                        // Alert.alert("Modal has been closed.");
                                    }}
                                >
                                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >

                                        <View style={{
                                            backgroundColor: '#000000aa',
                                            flex: 1
                                        }}>
                                            <View style={styles.modalForm}>
                                                <MyText textAlign={'center'} text={'EDIT'} isBold={true} />
                                                <MyFieldVert
                                                    topAlign={'center'}
                                                    bottomAlign={'flex-end'}
                                                    topComponent={<MyText isBold={true} fgColor={'white'} isBold={true} text={'Kondisi Tools'} />}
                                                    bottomComponent={
                                                        <MySelectHoriz option={[
                                                            { id: 0, label: 'aman' },
                                                            { id: 1, label: 'rusak' },
                                                        ]} textAlign={'center'} isFullWidth={true} value={kondisiTool} val={'kondisiTool'} name={'kondisiTool'} placeholder={'Stok'} />
                                                    }
                                                />
                                                <MyFieldVert
                                                    topComponent={<MyText isBold={true} fgColor={'white'} isBold={true} text={'Keterangan'} />}
                                                    bottomComponent={
                                                        <MyTextInput value={ketTool} isMultiLine={true} name={'ketTool'} placeholder={'keterangan'} />
                                                    }
                                                />

                                                <MyFieldHoriz
                                                    leftComponent={
                                                        <Button
                                                            color={'#5CC652'}
                                                            style={[styles.modalButton, {
                                                                backgroundColor: 'white',
                                                                marginVertical: .05 * wScreen,
                                                                marginRight: .02 * wScreen,
                                                            }]}
                                                            mode={'outlined'}
                                                            onPress={() => setModalVisible(!modalVisible)}
                                                        > Batal</Button>
                                                    }
                                                    rightComponent={
                                                        <Button
                                                            color={'white'}
                                                            style={[styles.modalButton, {
                                                                marginVertical: .05 * wScreen,
                                                                marginLeft: .02 * wScreen,
                                                                backgroundColor: '#5CC652',
                                                                textTransform: 'lowerCase'
                                                            }]}
                                                            // icon="save"
                                                            onPress={() => handleClickEdit()}
                                                        >Simpan</Button>
                                                    }
                                                />

                                            </View>
                                        </View>

                                    </ScrollView>
                                </Modal>



                            </View>

                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground >
    )
}

export default Component
