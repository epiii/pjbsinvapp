import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import {
    // Button,
    Modal, Alert, TextInput, ImageBackground, Dimensions, ScrollView, View, Text, Image
} from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import {
    Button,
    FAB, DataTable
} from 'react-native-paper';
import MyFieldHoriz from '../../../components/MyFieldHoriz'
import IconAD from 'react-native-vector-icons/AntDesign'
import IconFA from 'react-native-vector-icons/FontAwesome'
import MyNumberInput from '../../../components/MyNumberInput'
import MyFieldVert from '../../../components/MyFieldVert'
// import { Icon } from 'react-native-paper/lib/typescript/src/components/Avatar/Avatar';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const dataToolDikirim2_dummy = [
    { id: 1, nama: 'Start Proyek', deskripsi: 'DD/MM/YYY', status: 'baru' },
    { id: 2, nama: 'Selesai Proyek', deskripsi: 'DD/MM/YYY', status: 'baru' },
]
const dataToolDikirim_dummy = [
    { id: 1, nama: 'Proyek AAA', deskripsi: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 2, nama: 'Proyek BBB', deskripsi: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ', status: 'baru' },
    { id: 3, nama: 'Proyek CCC', deskripsi: 'ini adalah deskripsi proyek CCC, proyek pembangunan bla bla bla bla ', status: 'baru' },
]

const MyButton2 = ({ text, color, onPress, element }) => (
    <TouchableOpacity onPress={onPress}>
        <View style={[styles.labelVw, { backgroundColor: color }]}>
            {element}
        </View>
    </TouchableOpacity>
)

const MyText = ({ text, fgColor, bgColor, textAlign, isBold }) => (
    <View>
        <Text style={{
            fontWeight: isBold ? 'bold' : 'normal',
            fontSize: isBold ? (.042 * wScreen) : (.037 * wScreen),
            color: fgColor ? fgColor : 'white',
            textAlign: textAlign ? textAlign : 'justify',
            backgroundColor: bgColor ? bgColor : 'transparent'
        }}>{text}</Text>
    </View>
)

const MyButton = ({ text, fgColor, bgColor, onPress }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.myButton}>

            <Button
                compact={true}
                color={fgColor}
                style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}
                // style={[styles.myButton, { backgroundColor: bgColor }]}
                onPress={onPress}
            >{text}</Button>
        </View>
    )
}

const MyTextInput = ({ isMultiLine, value, name, textAlign, placeholder, label }) => (
    <TextInput
        style={[styles.txtInput, {
            textAlign: textAlign ? textAlign : 'left',
            textAlignVertical: isMultiLine ? 'top' : 'center',
            width: '100%'
        }]}
        numberOfLines={isMultiLine ? 3 : 1}
        multiline={isMultiLine ? true : false}

        name={name}
        value={value}
        placeholder={placeholder}
        placeholderTextColor="grey"
    />
)

const CardToolDikirim = ({ data }) => {
    const navigation = useNavigation();

    return (
        <View style={styles.toolProyCard}>

            <View style={styles.toolProyContentCard}>
                {data.map((item, index) => (
                    <TouchableOpacity
                        key={index}
                        onPress={() => navigation.navigate('DetailProyek', {
                            namaProyek: item.nama,
                            deskProyek: item.deskripsi,
                        })}
                    >
                        <View style={styles.vwListRencanaPro}>
                            <Text style={styles.txtListRencanaPro}>
                                {item.nama}
                            </Text>
                            <Text style={[styles.txtListRencanaPro, { fontWeight: 'normal', color: 'black' }]}>
                                {item.deskripsi}
                            </Text>
                        </View>
                    </TouchableOpacity>
                ))}
            </View>
        </View>
    )
}

function Component({ navigation }) {
    const [dataToolDikirim, setDataToolDikirim] = useState([])
    const [dataToolDikirim2, setDataToolDikirim2] = useState([])
    const [modalVisible, setModalVisible] = useState(false);
    const [jmlTool, setJmlTool] = useState(0);

    const [selectedRow, setSelectedRow] = useState({});
    const [selectedArr, setSelectedArr] = useState([]);
    const [availToolArr, setAvailToolArr] = useState([
        { inventoryNo: 'B001', name: 'Beko Y', qty: 100 },
        { inventoryNo: 'T002', name: 'Traktor Z', qty: 5 },
        { inventoryNo: 'FL03', name: 'Fork Lift X3', qty: 2 }
    ]);

    const MyTableTersedia = ({ openModalFormFuc }) => (
        <View style={styles.tableArea}>
            <Text style={styles.tableLabel}>Tools yang tersedia </Text>
            <DataTable style={styles.dataTable}>
                <DataTable.Header>
                    <DataTable.Title sortDirection='descending'>No Inv</DataTable.Title>
                    <DataTable.Title >Tool</DataTable.Title>
                    <DataTable.Title numeric>Qty  </DataTable.Title>
                    <DataTable.Title >Aksi</DataTable.Title>
                </DataTable.Header>

                {
                    availToolArr && availToolArr.map((data, i) => (
                        <DataTable.Row key={i} underlayColor={'red'} color={'red'}>
                            <DataTable.Cell>{data.inventoryNo}</DataTable.Cell>
                            <DataTable.Cell >{data.name}</DataTable.Cell>
                            <DataTable.Cell numeric>{data.qty} </DataTable.Cell>
                            <DataTable.Cell >
                                <MyButton2
                                    onPress={() => openModalFormFuc({
                                        inventoryNo: data.inventoryNo,
                                        name: data.name,
                                        qty: data.qty,
                                    })}
                                    element={<IconAD name={'pluscircle'} size={26} color={'#0B2E6A'} />}
                                    text={'plus'}
                                />
                            </DataTable.Cell>
                        </DataTable.Row>
                    ))
                }
                <DataTable.Pagination
                    page={1}
                    numberOfPages={3}
                    onPageChange={page => {
                        console.log(page);
                    }}
                    label="1-3 of 6"
                />
            </DataTable>
        </View>
    );

    const MyTableDibutuhkan = ({ selectedArr, openModalFormFuc }) => (
        <View style={styles.tableArea}>
            <Text style={styles.tableLabel}>Tools yang dibutuhkan </Text>
            <DataTable style={styles.dataTable}>
                <DataTable.Header>
                    <DataTable.Title sortDirection='descending'>No Inv</DataTable.Title>
                    <DataTable.Title >Tool</DataTable.Title>
                    <DataTable.Title numeric>Qty  </DataTable.Title>
                    <DataTable.Title >Aksi  </DataTable.Title>
                </DataTable.Header>
                {
                    selectedArr && selectedArr.map((data, i) => (
                        <DataTable.Row key={i}>
                            <DataTable.Cell>{data.inventoryNo}</DataTable.Cell>
                            <DataTable.Cell >{data.name}</DataTable.Cell>
                            <DataTable.Cell numeric>{data.qty + ' '} </DataTable.Cell>
                            <DataTable.Cell >
                                <MyButton2
                                    onPress={() => handleDeleteRow(data.inventoryNo)}
                                    // onPress={() => handleDeleteRow({
                                    //     inventoryNo: data.inventoryNo,
                                    //     // name: data.name,
                                    //     // qty: data.qty,
                                    // })}
                                    element={<IconAD name={'minuscircle'} size={26} color={'red'} />}
                                />
                            </DataTable.Cell>
                        </DataTable.Row>
                    ))
                }
                <DataTable.Pagination
                    page={1}
                    numberOfPages={3}
                    onPageChange={page => {
                        console.log(page);
                    }}
                    label="1-3 of 6"
                />
            </DataTable>
        </View>
    );

    const handleDeleteRow = (id) => {
        Alert.alert(
            'Konfirmasi ',
            'Anda yakin menghapus tool ini ?',
            [
                {
                    text: 'Tidak',
                    onPress: () => console.log('Cancel Pressed'),
                    style: 'cancel'
                },
                {
                    text: 'Ya', onPress: () => {
                        const newArr = selectedArr.filter(x => x.inventoryNo != id)
                        setSelectedArr(newArr)
                    }
                }
            ],
            { cancelable: false }
        );
        // console.log('selRow', selectedRow)
    }

    const handleAddTool = () => {
        setJmlTool(20)
        setModalVisible(false)
        console.log('selRow', selectedRow)
        setSelectedArr(selectedArr => [...selectedArr, selectedRow])
    }

    const handleConfirmAdd = () => {
        setJmlTool(20)
        setModalVisible(false)
        console.log('selRow', selectedRow)
        setSelectedArr[selectedRow]
    }

    const openModalForm = (param) => {
        console.log('param', param)
        setSelectedRow(param)
        setModalVisible(true)
    }

    useEffect(() => {
        setDataToolDikirim(dataToolDikirim_dummy)
        setDataToolDikirim2(dataToolDikirim2_dummy)
        return () => {
            // cleanup
        }
    }, [dataToolDikirim2, dataToolDikirim])

    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >
                            {/* 
                            <TouchableHighlight
                                style={styles.openButton}
                                onPress={() => {
                                    setModalVisible(true);
                                }}
                            >
                                <Text style={styles.textStyle}>Show Modal</Text>
                            </TouchableHighlight> */}

                            <MyFieldVert
                                topComponent={<MyText isBold={true} fgColor={'white'} text={'PIC'} />}
                                bottomComponent={
                                    <MyTextInput name={'noOrder'} placeholder={'Pilih PIC '} />
                                }
                            />
                            <MyFieldHoriz
                                leftComponent={<MyText isBold={true} fgColor={'white'} text={'Area Pekerjaan'} />}
                                rightComponent={
                                    <MyTextInput name={'areaPekerjaan'} placeholder={'Pilih Area Pekerjaan'} />
                                }
                            />
                            <MyFieldVert
                                topComponent={<MyText isBold={true} fgColor={'white'} text={'Cari Tools'} />}
                                bottomComponent={
                                    <View style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                        backgroundColor: 'white',
                                        borderRadius: .05 * wScreen,
                                        paddingHorizontal: .05 * wScreen,
                                    }}>
                                        <IconFA name={'search'} size={.05 * wScreen} color={'#959595'} />
                                        <MyTextInput textAlign={'left'} name={'cariTool'} placeholder={'Cari Tools Disini ...'} />
                                    </View>
                                }
                            />

                            <MyTableTersedia openModalFormFuc={openModalForm} />
                            <MyTableDibutuhkan selectedArr={selectedArr} />

                            <MyButton
                                fgColor={'white'}
                                bgColor={'#5CC652'}
                                onPress={() => alert('Simpan....')}
                                text={'Simpan'}
                            />

                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={modalVisible}
                                onRequestClose={() => {
                                    // Alert.alert("Modal has been closed.");
                                }}
                            >
                                <ScrollView contentContainerStyle={{ flexGrow: 1 }} >

                                    <View style={{
                                        backgroundColor: '#000000aa',
                                        flex: 1
                                    }}>
                                        <View style={styles.modalForm}>
                                            <MyText textAlign={'center'} text={'JUMLAH TOOLS'} isBold={true} />
                                            <MyFieldVert
                                                topAlign={'center'}
                                                bottomAlign={'flex-end'}
                                                topComponent={<MyText isBold={true} fgColor={'white'} isBold={true} text={''} />}
                                                bottomComponent={
                                                    <MyNumberInput textAlign={'center'} value={jmlTool} val={'jmlTool'} name={'jmlTool'} placeholder={'Stok'} />
                                                }
                                            />

                                            <MyFieldHoriz
                                                leftComponent={
                                                    <Button
                                                        color={'#5CC652'}
                                                        style={[styles.modalButton, {
                                                            backgroundColor: 'white',
                                                            marginVertical: .05 * wScreen,
                                                            marginRight: .02 * wScreen,
                                                        }]}
                                                        mode={'outlined'}
                                                        onPress={() => setModalVisible(!modalVisible)}
                                                    > Batal</Button>
                                                }
                                                rightComponent={
                                                    <Button
                                                        color={'white'}
                                                        style={[styles.modalButton, {
                                                            marginVertical: .05 * wScreen,
                                                            marginLeft: .02 * wScreen,
                                                            backgroundColor: '#5CC652',
                                                            textTransform: 'lowerCase'
                                                        }]}
                                                        // icon="save"
                                                        onPress={() => handleAddTool()}
                                                    >Tambah</Button>
                                                }
                                            />

                                        </View>
                                    </View>

                                </ScrollView>
                            </Modal>


                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground >
    )
}

export default Component
