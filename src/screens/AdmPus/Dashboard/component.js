import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FAB } from 'react-native-paper';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const CardRenProy = ({ tahun }) => {
    const navigation = useNavigation();

    return (
        <View style={{ width: '100%' }}>
            <View style={styles.renProyTopCard}>
                <Text style={styles.txtHeadListRencanaPro}>
                    Rencana Proyek {tahun}
                </Text>
            </View>

            <View style={styles.renProyContentCard}>
                <TouchableOpacity
                    onPress={() => navigation.navigate('DetailProyek', {
                        namaProyek: 'proyek AAA',
                        deskProyek: 'ini adalah deskripsi proyek AAA, proyek pembangunan bla bla bla bla ',
                    })}
                >
                    <View style={styles.vwListRencanaPro}>
                        <Text style={styles.txtListRencanaPro}>
                            1. Proyek AAA
                            </Text>
                        <Label fgColor={'white'} bgColor={'#EC3247'} text={'BARU'} />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('DetailProyek', {
                    namaProyek: 'proyek BBB',
                    deskProyek: 'ini adalah deskripsi proyek BBB, proyek pembangunan bla bla bla bla ',
                })}
                >
                    <View style={styles.vwListRencanaPro}>
                        <Text style={styles.txtListRencanaPro}>
                            2. Proyek BBB
                            </Text>
                        <Label fgColor={'white'} bgColor={'#EC3247'} text={'BARU'} />
                    </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => navigation.navigate('DetailProyek', {
                    namaProyek: 'proyek ccc',
                    deskProyek: 'ini adalah deskripsi proyek ccc, proyek pembangunan bla bla bla bla ',
                })}
                >
                    <View style={styles.vwListRencanaPro}>
                        <Text style={styles.txtListRencanaPro}>
                            3. Proyek CCC
                            </Text>
                        <Label fgColor={'white'} bgColor={'#EC3247'} text={'BARU'} />
                        {/* <Button
                            color='white'
                            style={styles.btnListRencanaPro}
                        >baru</Button> */}
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const ChartProyek_dummy = ({ total }) => (
    <Image
        style={[styles.iconImg, styles.chartProySize]}
        source={IMAGES.CHART_CIRCLE}
    // source={require("../../assets/images/chart_circle.png")}
    />
)

const ChartHoriz_dummy = ({ onPress, img, keterangan, jumlah }) => (
    <TouchableHighlight underlayColor={'#043353'} onPress={onPress}>
        <View style={styles.chartHorizVw}>
            <Image
                style={{ width: '100%' }}
                source={img}
            />
            <Text style={styles.chartHorizTxt}>{keterangan}</Text>
            {/* <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{jumlah}</Text> */}
        </View>
    </TouchableHighlight>
)

const ChartRealisasi_dummy = ({ rencana, realisasi }) => (
    <View style={styles.chartRenRealVw}>
        <Image
            style={styles.chartRenRealImg}
            source={IMAGES.CHART_HISTOGRAM}
        // source={require("../../assets/images/chart_histogram.png")}
        />
        <View style={styles.chartRenRealRow}>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'left' }]}>{'Rencana\nProyek'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{rencana} %</Text>
            </View>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'right' }]}>{'Realisasi\nProyek'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'right' }]}>{realisasi} %</Text>
            </View>
        </View>
    </View>
)

const ChartProyek = ({ total }) => (
    <ProgressChart
        hideLegend={true}
        data={{
            labels: ["Andi",],
            data: [0.5]
        }}
        width={0.7 * wScreen} // from react-native
        height={.4 * wScreen}
        // height={220}
        chartConfig={{
            // backgroundGradientFrom: '#0B2E6A',
            // backgroundGradientTo: 'purple',
            // color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0.0,
            color: (opacity = 1) => `rgba(236, 50, 71, ${opacity})`,
            strokeWidth: 1, // optional, default 3
        }}
    // backgroundColor="green"
    // paddingLeft="15"
    // absolute
    // style={{
    //     marginVertical: 8,
    //     borderRadius: 16
    // }}
    />
)

const CardProyek = ({ total }) => (
    <View style={styles.proyCard}>
        <Text style={styles.proyTxt}>
            Total Proyek
        </Text>
        <View style={styles.proyRow}>
            <Text style={styles.proyTotTxt}>{total}</Text>
            <Image
                style={[styles.iconImg, styles.proyIconSize]}
                source={IMAGES.KUNCI_PAS}
            // source={require("../../assets/icons/kunci_pas.png")}
            />
        </View>
    </View>
)

function Component({ navigation }) {
    return (
        <ImageBackground
            // source={IMAGES.PETIR}
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >
                            <View style={styles.vwRow}>
                                <View style={[styles.vwItem, {
                                    // backgroundColor: 'red',
                                }]}>
                                    <CardProyek total={10} />
                                </View>
                                <View style={[styles.vwItem, {
                                    // backgroundColor: 'yellow',
                                }]}>
                                    <ChartProyek_dummy />
                                </View>
                            </View>
                            <CardRenProy tahun={2020} />
                            <ChartRealisasi_dummy rencana={40} realisasi={60} />
                            <ChartHoriz_dummy
                                onPress={() => navigation.navigate('PersiapanToolForm')}
                                img={IMAGES.CHART_TOOL_DIKALIBRASI}
                                keterangan={'Tools Yang Telah dikalibrasi'}
                                jumlah={30}
                            />
                            <ChartHoriz_dummy
                                onPress={() => navigation.navigate('JWaktuPinjamToolForm')}
                                img={IMAGES.CHART_TOOL_DIPINJAM}
                                keterangan={'Tools paling Lama Dipinjam'}
                                jumlah={30}
                            />
                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
