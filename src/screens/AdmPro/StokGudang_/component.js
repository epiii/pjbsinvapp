import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TextInput, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button, FAB, DataTable } from 'react-native-paper';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const MyTable = () => (
    <DataTable style={styles.dataTable}>
        <DataTable.Header>
            <DataTable.Title>No Inv.</DataTable.Title>
            <DataTable.Title numeric>Tool</DataTable.Title>
            <DataTable.Title numeric>Qty</DataTable.Title>
            <DataTable.Title numeric>Utilization</DataTable.Title>
        </DataTable.Header>

        <DataTable.Row>
            <DataTable.Cell>Frozen yogurt</DataTable.Cell>
            <DataTable.Cell numeric>159</DataTable.Cell>
            <DataTable.Cell numeric>6.0</DataTable.Cell>
            <DataTable.Cell numeric>6.0</DataTable.Cell>
        </DataTable.Row>

        <DataTable.Row>
            <DataTable.Cell>Ice cream sandwich</DataTable.Cell>
            <DataTable.Cell numeric>237</DataTable.Cell>
            <DataTable.Cell numeric>8.0</DataTable.Cell>
            <DataTable.Cell numeric>8.0</DataTable.Cell>
        </DataTable.Row>

        <DataTable.Row>
            <DataTable.Cell>Ice cream sandwich</DataTable.Cell>
            <DataTable.Cell numeric>237</DataTable.Cell>
            <DataTable.Cell numeric>8.0</DataTable.Cell>
            <DataTable.Cell numeric>8.0</DataTable.Cell>
        </DataTable.Row>

        <DataTable.Pagination
            page={1}
            numberOfPages={3}
            onPageChange={page => {
                console.log(page);
            }}
            label="1-3 of 6"
        />
    </DataTable>
);

function Component({ navigation, route }) {
    // React.useEffect(() => {
    //     if (route.params?.namaProyek) {
    //         // For example, send the post to the server
    //     }
    // }, [route.params?.namaProyek]);


    return (
        <SafeAreaView
            style={styles.container}
        >
            <ScrollView
                contentContainerStyle={{ flexGrow: 1 }}
            >
                <View style={{
                    height: 1.3 * hScreen
                }}>
                    <Image style={styles.bgImage} source={IMAGES.PETIR} />
                    <Image style={[styles.bgImage, styles.overlayImage]} source={IMAGES.PEMBANGKIT} />

                    <View style={styles.content}>

                        <View style={styles.fieldArea} >
                            <TextInput
                                style={styles.txtKeterangan}
                                underlineColorAndroid="transparent"
                                placeholder="Cari Tool disini ..."
                                placeholderTextColor="grey"
                                multiline={true}
                            />
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: .04 * wScreen, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1 }}>
                                <Text style={{ flexWrap: 'wrap', color: 'white', textAlign: 'justify' }}>{
                                    // route.params?.namaProyek
                                }</Text>
                            </View>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: .04 * wScreen, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1, }}>
                                <Image style={styles.thumbImg} source={IMAGES.TOOL_1} />
                            </View>
                            <View style={{ flex: 1 }}>
                                <Image style={styles.thumbImg} source={IMAGES.TOOL_2} />
                            </View>
                        </View>

                        <View style={{ flex: 1, flexDirection: 'row', paddingTop: .04 * wScreen, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1, }}>
                                <Image style={styles.thumbImg} source={IMAGES.TOOL_3} />
                            </View>
                            <View style={{ flex: 1 }}>
                                <Image style={styles.thumbImg} source={IMAGES.TOOL_4} />
                            </View>
                        </View>


                        {/* <View style={{ flex: 1, flexDirection: 'row', paddingTop: .04 * wScreen, justifyContent: 'space-between' }}>
                            <View style={{ flex: 1, }}>
                                <Button
                                    color='white'
                                    style={styles.btnListRencanaPro}
                                    onPress={() => alert('sedang memproses ...')}
                                >Setujui</Button>
                            </View>
                            <View style={{ flex: 1, }}>
                                <Button
                                    color='white'
                                    style={[styles.btnListRencanaPro, { backgroundColor: '#EC3247' }]}
                                    onPress={() => alert('sedang memproses ...')}
                                >Tolak
                                </Button>
                            </View>
                        </View> */}

                    </View>
                </View>

            </ScrollView>
        </SafeAreaView>
    )
}

export default Component
