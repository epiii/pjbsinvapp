import { Dimensions, StyleSheet } from 'react-native';
const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#0B2E6A'
    },
    toolProyCard: {
        width: '100%',
        paddingVertical: .03 * wScreen,
    },
    toolProyContentCard: {
        backgroundColor: 'white',
        borderRadius: .05 * wScreen,
        flexDirection: 'column',
        justifyContent: 'center',
        paddingHorizontal: .05 * wScreen,
        paddingVertical: .05 * wScreen,
        // paddingBottom: .05 * wScreen,
    },
    labelVw: {
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: .7 * wScreen,
    },
    labelTxt: {
        padding: .006 * wScreen,
        color: 'white',
        textAlign: 'center'
    },
    button: {
        marginVertical: .1 * wScreen,
    },
    dataTable: {
        marginTop: 5,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    bgImage: {
        width: '100%',
        height: '100%',
        // flex: 1,
        // resizeMode: 'cover', // or 'stretch'
    },
    chartProySize: {
        height: .45 * wScreen,
        width: .45 * wScreen,
    },
    chartRenRealImg: {
        width: '100%'
    },
    chartRenRealRow: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    chartRenRealItem: {

    },
    chartRenRealTxt: {
        color: 'white',
        marginTop: .04 * wScreen,
        marginBottom: .001 * wScreen,
        fontSize: .05 * wScreen,
        fontWeight: 'bold'
    },
    chartHorizTxt: {
        color: 'white',
        marginTop: -.02 * wScreen,
        marginBottom: .001 * wScreen,
        fontSize: .05 * wScreen,
        fontWeight: 'bold'
    },
    chartRenRealTxtNum: {
        color: 'white',
        marginVertical: .0001 * wScreen,
        fontSize: .10 * wScreen,
        fontWeight: 'bold'
    },
    chartRenRealVw: {
        paddingTop: .01 * wScreen,
        paddingBottom: .01 * wScreen,
        // height: .08 * wScreen,
        // width: .08 * wScreen,
    },
    chartHorizVw: {
        paddingVertical: .003 * wScreen,
    },
    fab: {
        position: 'absolute',
        margin: 16,
        backgroundColor: '#043353',
        right: 0,
        left: 0,
        bottom: -0.06 * wScreen,
        zIndex: 99
    },
    toolProyTopCard: {
        justifyContent: 'center',
        backgroundColor: 'white',
        // backgroundColor: '#C4C4C4',
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        flexDirection: 'column',
        paddingHorizontal: 10,
        paddingTop: .02 * wScreen,
    },
    proyCard: {
        padding: .04 * wScreen,
        backgroundColor: 'white',
        height: .40 * wScreen,
        width: .40 * wScreen,
        borderRadius: 20,
        flexDirection: 'column'
    },
    proyTxt: {
        color: '#EC3247',
        fontSize: .057 * wScreen,
        fontWeight: 'bold',
        justifyContent: 'flex-start',
        textAlign: 'center',
    },
    proyTotTxt: {
        justifyContent: 'flex-start',
        padding: .04 * wScreen,
        fontSize: .1 * wScreen,
        color: 'black',
        fontWeight: 'bold'
    },
    vwRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-end',
        marginBottom: .07 * wScreen,
    },
    vwItem: {
        // height: '50%',
        // width: '50%',
    },
    proyRow: {
        flexDirection: 'row',
    },
    txtHeadListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        textAlign: 'justify',
        fontSize: .05 * wScreen,
        textAlign: 'center',
        color: '#EC3247',
        // color: 'black',
        fontWeight: 'bold'
    },
    myButton: {
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: .06 * wScreen,
        // paddingHorizontal: .04 * wScreen,
        justifyContent: 'center',
        width: '100%'
    },
    btnListRencanaPro: {
        alignItems: 'center',
        alignSelf: 'center',
        // backgroundColor: '#EC3247',
        color: 'white',
        borderRadius: .06 * wScreen,
        paddingVertical: .01 * wScreen,
        paddingHorizontal: .04 * wScreen,
        justifyContent: 'center',
    },
    fieldArea: {
        // marginVertical: .01 * wScreen,
    },
    dropDown: {
        backgroundColor: 'white',
        borderRadius: 20,
        padding: 20,
    },
    tableArea: {
        paddingVertical: .03 * wScreen,
    },
    tableLabel: {
        color: 'white',
        fontWeight: 'bold',
        paddingVertical: .01 * wScreen,
    },
    fieldLabel: {
        // paddingVertical: .01 * wScreen,
        color: 'white'
    },
    fieldAreaHoriz: {
        // backgroundColor: 'yellow',
        justifyContent: 'space-between',
        alignItems: 'center',
        flexDirection: 'row',
        marginVertical: .01 * wScreen,
    }, modalForm: {
        backgroundColor: '#0B2E6A',
        marginHorizontal: .08 * wScreen,
        marginVertical: .2 * wScreen,
        padding: .05 * wScreen,
        borderRadius: .035 * wScreen,
        flex: 1,
    },
    leftComponent: {
        flex: 4,
        // backgroundColor: 'green'
    },
    rightComponent: {
        flex: 3
    },
    fieldAreaVert: {
        justifyContent: 'space-between',
        // alignItems: 'center',
        marginVertical: .01 * wScreen,
    },
    topComponent: {
        paddingVertical: .01 * wScreen,
        // flex: 4,
        // backgroundColor: 'green'
    },
    bottomComponent: {
        // flex: 3
    },
    txtInput: {
        backgroundColor: 'white',
        borderRadius: .06 * wScreen,
        paddingHorizontal: .05 * wScreen,
    },
    safeAreaView: {
        // alignItems: 'center',
        // flex: 1,
        justifyContent: 'space-around',
        padding: .05 * wScreen
    },
    txtListRencanaPro: {
        justifyContent: 'center',
        padding: .02 * wScreen,
        color: '#EC3247',
        fontWeight: 'bold',
    },
    vwListRencanaPro: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    iconImg: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center'
    },
    proyIconSize: {
        width: .10 * wScreen,
        height: .10 * wScreen
    },
    overlayImage: {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        justifyContent: "center",
        alignItems: "center",
    },
    content: {
        position: "absolute",
        // top: 0,
        // right: 0,
        // bottom: 0,
        // left: 0,
        justifyContent: "center",
        alignItems: "center",
    },
    bigBlue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30,
    },
    mainText: {
        color: 'white'
    }
});
export default styles