import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { Button } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { FAB } from 'react-native-paper';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const dataTool_dummy = [
    { id: 1, nama: 'Tool AAA', kategori: 'Utilisasi', isCalibrated: true, },
]

const ListTool = ({ data }) => (
    <View>{
        data.map((item, index) => (
            <View key={index} style={{ flex: 1, flexDirection: 'row', paddingTop: .04 * wScreen, justifyContent: 'space-between' }}>
                <View style={{ flex: 1, }}>
                    <Text>{data.nama}</Text>
                    <Image style={styles.thumbImg} source={IMAGES.TOOL_1} />
                </View>
            </View>
        ))}
    </View>
)

function Component({ navigation }) {
    const [dataTool, setDataTool] = useState([])

    useEffect(() => {
        setDataTool(dataTool_dummy)
        return () => {
            // cleanup
        }
    }, [dataTool])

    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >

                            <ListTool data={dataTool} />
                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
