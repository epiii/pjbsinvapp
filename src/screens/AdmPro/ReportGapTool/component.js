import React from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity, ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';
import { Button, FAB, DataTable } from 'react-native-paper';
import {
    ProgressChart,
    LineChart,
} from "react-native-chart-kit";
import Icon from 'react-native-vector-icons/FontAwesome'

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)


const ChartProyek_dummy = ({ total }) => (
    <Image
        style={[styles.iconImg, styles.chartProySize]}
        source={require("../../../assets/images/chart_circle.png")}
    />
)

const ChartHoriz_dummy = ({ onPress, img, keterangan, jumlah }) => (
    <TouchableHighlight underlayColor={'#043353'} onPress={onPress}>
        <View style={styles.chartHorizVw}>
            <Image
                style={{ width: '100%' }}
                source={img}
            />
            <Text style={styles.chartHorizTxt}>{keterangan}</Text>
            {/* <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{jumlah}</Text> */}
        </View>
    </TouchableHighlight>
)

const ChartRealisasi_dummy = ({ rencana, realisasi }) => (
    <View style={styles.chartRenRealVw}>
        <Image
            style={styles.chartRenRealImg}
            source={require("../../../assets/images/chart_histogram.png")}
        />
        <View style={styles.chartRenRealRow}>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'left' }]}>{'Rencana\nProyek'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'left' }]}>{rencana} %</Text>
            </View>
            <View style={styles.chartRenRealItem}>
                <Text style={[styles.chartRenRealTxt, { textAlign: 'right' }]}>{'Realisasi\nProyek'}</Text>
                <Text style={[styles.chartRenRealTxtNum, { textAlign: 'right' }]}>{realisasi} %</Text>
            </View>
        </View>
    </View>
)

const ChartToolStock = ({ total }) => {
    const data = {
        labels: ["January", "February", "March", "April", "May", "June"],
        datasets: [
            {
                data: [20, 45, 28, 80, 99, 43],
                color: (opacity = 1) => `rgba(134, 65, 244, ${opacity})`, // optional
                strokeWidth: 2 // optional
            }
        ],
        legend: ["Jumlah Tools"] // optional
    };

    const chartConfig = {
        backgroundColor: "white",
        backgroundGradientFrom: "white",
        backgroundGradientTo: "white",
        decimalPlaces: 2, // optional, defaults to 2dp
        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        labelColor: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
        style: {
            borderRadius: 30,
        },
        propsForDots: {
            r: "6",
            strokeWidth: "2",
            stroke: "#ffa726"
        }
    }

    return (
        <LineChart
            data={data}
            width={wScreen}
            height={220}
            chartConfig={chartConfig}
        />
    )
}

const ChartProyek = ({ total }) => (
    <ProgressChart
        hideLegend={true}
        data={{
            labels: ["Andi",],
            data: [0.5]
        }}
        width={0.7 * wScreen} // from react-native
        height={.4 * wScreen}
        // height={220}
        chartConfig={{
            // backgroundGradientFrom: '#0B2E6A',
            // backgroundGradientTo: 'purple',
            // color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
            backgroundGradientFromOpacity: 0,
            backgroundGradientToOpacity: 0.0,
            color: (opacity = 1) => `rgba(236, 50, 71, ${opacity})`,
            strokeWidth: 1, // optional, default 3
        }}
    // backgroundColor="green"
    // paddingLeft="15"
    // absolute
    // style={{
    //     marginVertical: 8,
    //     borderRadius: 16
    // }}
    />
)

const CardMini = ({ name, total }) => (
    <View style={styles.proyCard}>
        <Text style={styles.proyTxt}>
            {name}
        </Text>
        <View style={styles.proyRow}>
            <Text style={styles.proyTotTxt}>{total}</Text>
        </View>
    </View>
)
const TransaksiToolTbl = () => (
    <DataTable style={styles.dataTable}>
        <DataTable.Header>
            <DataTable.Title>Proj.</DataTable.Title>
            <DataTable.Title numeric>Mulai</DataTable.Title>
            <DataTable.Title numeric>Selesai</DataTable.Title>
            <DataTable.Title numeric>Status</DataTable.Title>
        </DataTable.Header>

        <DataTable.Row>
            <DataTable.Cell numeric>Project AAA</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>Done</DataTable.Cell>
        </DataTable.Row>

        <DataTable.Row>
            <DataTable.Cell numeric>Project AAA</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>Progress</DataTable.Cell>
        </DataTable.Row>

        <DataTable.Row>
            <DataTable.Cell numeric>Project AAA</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>01/12/2019</DataTable.Cell>
            <DataTable.Cell numeric>Cancel</DataTable.Cell>
        </DataTable.Row>

        <DataTable.Pagination
            page={1}
            numberOfPages={3}
            onPageChange={page => {
                console.log(page);
            }}
            label="1-3 of 6"
        />
    </DataTable>
);

function Component({ navigation }) {
    return (
        <ImageBackground
            source={require("../../../assets/images/petir.png")}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={require("../../../assets/images/pembangkit.png")}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >
                            <View style={styles.section}>
                                <Text style={styles.sectionSub}>Laporan Gap Tools</Text>

                                <TouchableOpacity onPress={() => alert('on development...')}>
                                    <View style={styles.filter}>
                                        <Text style={[styles.sectionSub, { marginRight: .02 * wScreen, color: 'black' }]}>Semua</Text>
                                        <Icon name={'chevron-down'} />
                                    </View>
                                </TouchableOpacity>
                            </View>

                            <ChartToolStock tahun={2020} />

                            <View style={styles.vwRow}>
                                <View style={[styles.vwItem, {
                                    // backgroundColor: 'red',
                                }]}>
                                    <CardMini name={'Tools\nRusak'} total={10} />
                                </View>
                                <View style={[styles.vwItem, {
                                    // backgroundColor: 'red',
                                }]}>
                                    <CardMini name={'Tools Perlu Kalibrasi'} total={4} />
                                </View>
                            </View>
                            <TransaksiToolTbl />
                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
