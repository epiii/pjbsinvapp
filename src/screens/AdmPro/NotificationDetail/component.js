import React, { useEffect, useState } from 'react'
import { SafeAreaView } from 'react-native-safe-area-context';
import { ImageBackground, Dimensions, ScrollView, View, Text, Image } from 'react-native'
import styles from './styles'
import { IMAGES } from '../../../constants'
import { ProgressChart } from 'react-native-chart-kit'
import { Button, FAB, List, DataTable } from 'react-native-paper';
import { TouchableOpacity, TouchableHighlight } from 'react-native-gesture-handler';
import { useNavigation } from '@react-navigation/native';

const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const Label = ({ fgColor, bgColor, text }) => (
    <View style={[styles.btnListRencanaPro, { backgroundColor: bgColor }]}>
        <Text style={{ color: fgColor }}>{text}</Text>
    </View>
)

const CardDetail = ({ notifTitle, notifDesc }) => {
    return (
        <View style={styles.cardDetail}>
            <Text style={styles.notifTitle}>{notifTitle}</Text>
            <Text style={styles.notifDesc}>{notifDesc}</Text>
        </View>
    );
}

function Component({ navigation, route }) {
    useEffect(() => {
        if (route.params?.notifTitle) {
        }
    }, [route.params?.notifTitle]);

    return (
        <ImageBackground
            source={IMAGES.PETIR}
            style={[styles.bgImage, {
                backgroundColor: '#0B2E6A',
            }]}
        >
            <ImageBackground
                source={IMAGES.PEMBANGKIT}
                style={styles.bgImage}
            >
                <SafeAreaView style={styles.safeAreaView}>
                    <ScrollView contentContainerStyle={{ flexGrow: 1 }} >
                        <View style={{ flex: 1 }} >
                            {/* <MyList data={dataNotification} /> */}
                            <CardDetail
                                notifTitle={route.params?.notifTitle}
                                notifDesc={route.params?.notifDesc}
                            />
                        </View>
                    </ScrollView>
                </SafeAreaView>

            </ImageBackground>
        </ImageBackground>
    )
}

export default Component
