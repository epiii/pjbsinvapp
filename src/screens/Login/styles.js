import { StyleSheet } from 'react-native';
import { Dimensions } from 'react-native'
const wScreen = Dimensions.get("window").width
const hScreen = Dimensions.get("window").height

const styles = StyleSheet.create({
    container: {
        // backgroundColor: 'red',
        flex: 1,
        flexDirection: 'column',
        padding: .05 * wScreen
    },
    sectionTop: {
        height: .55 * hScreen,
        justifyContent: 'center',
        alignItems: 'center',
        // backgroundColor: 'pink',
    },
    logoArea: {
        flex: 3,
        // backgroundColor: 'green',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    bgImage: {
        // width: '100%',
        // height: '100%',
        // flex: 1,
        // resizeMode: 'cover', // or 'stretch'
    },
    txtInput: {
        // marginTop: 20,
        backgroundColor: 'white',
        borderRadius: 10,
        opacity: .6,
        textAlign: 'center',
        fontWeight: 'bold',
    },
    dataTable: {
        marginTop: 5,
        backgroundColor: 'white',
        borderRadius: 20,
    },
    fieldArea: {
        // marginTop: 20
    },
    txtHeadListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        fontSize: 15,
        textAlign: 'center',
        color: 'black',
        fontWeight: 'bold'
    },
    safeAreaView: {
        // justifyContent: 'space-around',
        // padding: .05 * wScreen
    },
    btnListRencanaPro: {
        // marginTop: 20,
        // marginBottom: 400,
        backgroundColor: '#FFAC30',
        borderRadius: 10,
        color: 'white',
        margin: 5,
        // padding: -5
    },
    txtListRencanaPro: {
        justifyContent: 'center',
        padding: 10,
        textAlign: 'center',
        color: 'black',
        // fontWeight: 'bold',
        // margin:-5,
        // height:10
    },
    vwListRencanaPro: {
        flexDirection: 'row'
    },

    cardListRencanaPro: {
        flex: 1,
        justifyContent: 'space-between',
        width: '80%',
        padding: 10,
        backgroundColor: 'white',
        borderBottomLeftRadius: 20,
        borderBottomRightRadius: 20,
    },
    iconImg: {
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center'
    },
    overlayImage: {
        position: "absolute",
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        justifyContent: "center",
        alignItems: "center",
    },
    vwStatus: {
        flex: 1,
        backgroundColor: '#FFAC30',
        // marginRight: 20,
        padding: 5,
        alignSelf: 'center',
        alignItems: 'center',
        borderRadius: 20
    },
    cardContent: {
        padding: 10,
        marginTop: 20,
        backgroundColor: 'white',
        borderRadius: 20,
        // flexDirection: 'column'
    },
    listRow: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 5,
    },
    listField: {
        textAlign: 'right',
        color: 'black',
        alignItems: 'center',
        alignSelf: 'center',
        fontSize: .04 * wScreen,
    },
    listLabel: {
        color: '#EC3247',
        fontWeight: 'bold',
        fontSize: .05 * wScreen,
        alignItems: 'center',
        alignSelf: 'center',
    },
    txtStatus: {
        color: 'white',
        textAlign: 'right'
    },
    txtLabel: {
        fontSize: .05 * wScreen,
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',

        marginVertical: .05 * wScreen,
        width: '100%',
    },
    sectionBottom: {
        height: .35 * hScreen,
        // backgroundColor: 'yellow',
        justifyContent: 'space-around'
    },

    content: {
        width: '100%',
        position: "absolute",
        marginBottom: 300,
        paddingBottom: 100,
        // height:4000,
        // top: 0,
        // right: 0,
        // bottom: 0,
        // left: 0,
        justifyContent: "center",
        // alignItems: "center",
    },
    scrollView: {
        flex: 1
        // height: 3 * wScreen,
    },
    bigBlue: {
        color: 'blue',
        fontWeight: 'bold',
        fontSize: 30,
    },
    mainText: {
        color: 'white'
    }
    // red: {
    //     color: 'red',
    // },
});
export default styles