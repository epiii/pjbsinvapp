import React from 'react'
import {
    HomeStackAdmPusScreen,
    ToolStackScreen,
    MoreStackScreen,
    AccountStackScreen,
    NotificationStackScreen,
    AuthStackScreen
} from '../../configs/Routes'
// import MaterialCommunityIcons  from 'react-native-vector-icons/MaterialCommunityIcons '
import IconF from 'react-native-vector-icons/Feather'
import IconI from 'react-native-vector-icons/Ionicons'
import IconAD from 'react-native-vector-icons/AntDesign'
import IconFA from 'react-native-vector-icons/FontAwesome5'
import AddButton from '../AddButton'

import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs'
import styles from './styles'
const Tab = createMaterialBottomTabNavigator()

const Component = () => (
    <Tab.Navigator
        initialRouteName="Dashboard"
        activeColor="#043353"
        inactiveColor="grey"
        barStyle={{ backgroundColor: 'white' }}
    >
        <Tab.Screen
            name="Dashboard"
            component={HomeStackAdmPusScreen}
            options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color }) => (
                    <IconF name="home" color={color} size={26} />
                ),
            }}
        />
        <Tab.Screen
            name="Notifications"
            component={NotificationStackScreen}
            options={{
                tabBarLabel: 'Notification',
                tabBarIcon: ({ color }) => (
                    <IconF name="bell" color={color} size={26} />
                ),
            }}
        />
        <Tab.Screen
            name="Tool"
            component={ToolStackScreen}
            options={{
                tabBarLabel: 'Tools',
                tabBarIcon: ({ color }) => (
                    <IconFA name="boxes" color={color} size={21} />
                ),
            }}
        />
        <Tab.Screen
            name="More"
            component={MoreStackScreen}
            options={{
                tabBarLabel: 'More',
                tabBarIcon: ({ color }) => (
                    <IconAD name="appstore-o" color={color} size={26} />
                ),
            }}
        />
        {/* <Tab.Screen
                name="Account"
                component={AccountStackScreen}
                options={{
                    tabBarLabel: 'Account',
                    tabBarIcon: ({ color }) => (
                        <Icon name="home" color={color} size={26} />
                    ),
                }}
            /> */}
    </Tab.Navigator>
)

export default Component