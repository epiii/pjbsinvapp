import React from 'react'
import { TouchableHighlight, View, } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome'
import styles from './style'
import { useNavigation } from '@react-navigation/native';

const component = ({ bgColor, fgColor, onPress, icon }) => {
    const nav = useNavigation()
    return (
        <TouchableHighlight
            underlayColor={'#7C4372aa'}
            // onPress={() => nav.navigate('AccountStack', { screen: 'Profile' })}
            onPress={() => nav.navigate(onPress)}
        // onPress={onPress}
        >
            <View style={[styles.button, { backgroundColor: bgColor ? bgColor : 'transparent' }]}>
                <Icon
                    name={icon}
                    size={26}
                    color={fgColor}
                />
            </View>
        </TouchableHighlight>
    )
}

export default component
